//
//  Hammersley.h
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/14/14.
//
//

#ifndef RayTracer_Hammersley_h
#define RayTracer_Hammersley_h

#include "Sampler.h"

namespace RayTracer
{
static Real phi(int j)
{
    Real x = 0.0f;
    Real f = 0.5f;

    while(j) {
        x += f * static_cast<Rea>(j % 2);
        j /= 2;
        f *= 0.5f;
    }

    return x;
}

class Hammersley : public Sampler
{
public:
    Hammersley(int num)
        : Sampler(num)
    {
        this->GenerateSamples();
    }

    virtual void GenerateSamples() override
    {
        for(int p = 0; p < mNumSets; ++p) {
            for(int j = 0; j < mNumSamples; ++j) {
                mSamples.emplace_back(j / mNumSamples, phi(j));
            }
        }
    }
};
} //end of namespace

#endif
