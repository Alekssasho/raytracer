//
//  Regular.h
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/14/14.
//
//

#ifndef RayTracer_Regular_h
#define RayTracer_Regular_h

#include "Sampler.h"

namespace RayTracer
{
class Regular : public Sampler
{
public:
    Regular(int num)
        : Sampler(num)
    {
        this->GenerateSamples();
    }

    Regular(int num, int numSets)
        : Sampler(num, numSets)
    {
        this->GenerateSamples();
    }

    virtual void GenerateSamples() override
    {
        int n = sqrt(mNumSamples);

        for(int p = 0; p < mNumSets; ++p) {
            for(int j = 0; j < n; ++j) {
                for(int k = 0; k < n; ++k) {
                    mSamples.emplace_back((k + 0.5f) / n, (j + 0.5f) / n);
                }
            }
        }
    }
};
} //end of namespace

#endif
