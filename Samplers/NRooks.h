//
//  NRooks.h
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/14/14.
//
//

#ifndef RayTracer_NRooks_h
#define RayTracer_NRooks_h

#include "Sampler.h"

namespace RayTracer
{
class NRooks : public Sampler
{
public:
    NRooks(int num)
        : Sampler(num)
    {
        this->GenerateSamples();
    }

    virtual void GenerateSamples() override
    {
        for(int p = 0; p < mNumSets; ++p) {
            for(int j = 0; j < mNumSamples; ++j) {
                mSamples.emplace_back((j + RandReal()) / mNumSamples, (j + RandReal()) / mNumSamples);
            }

            this->ShuffleXCoordinates();
            this->ShuffleYCoordinates();
        }
    }
};
} //end of namespace

#endif
