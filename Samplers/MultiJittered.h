//
//  MultiJittered.h
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/14/14.
//
//

#ifndef RayTracer_MultiJittered_h
#define RayTracer_MultiJittered_h

#include "Sampler.h"

namespace RayTracer
{

inline Real RandReal(Real l, Real h)
{
    return RandReal() * (h - l) + l;
}

inline int RandInt(int l, int h)
{
    return RandReal(0, h - l) + l;
}

class MultiJittered : public Sampler
{
public:
    MultiJittered(int num)
        : Sampler(num)
    {
        this->GenerateSamples();
    }

    virtual void GenerateSamples() override
    {
        int n = sqrt(mNumSamples);
        Real subcellWidth = 1.0f / static_cast<Real>(mNumSamples);

        mSamples.resize(mNumSamples * mNumSets);

        for(int p = 0; p < mNumSets; ++p) {
            for(int i = 0; i < n; ++i) {
                for(int j = 0; j < n; ++j) {
                    int index = i * n + j + p * mNumSamples;
                    mSamples[index].x = (i * n + j) * subcellWidth + RandReal(0, subcellWidth);
                    mSamples[index].y = (j * n + i) * subcellWidth + RandReal(0, subcellWidth);
                }
            }
        }
        for(int p = 0; p < mNumSets; ++p) {
            for(int i = 0; i < n; ++i) {
                for(int j = 0; j < n; ++j) {
                    int k = RandInt(j, n - 1);
                    int index = i * n + j + p * mNumSamples;
                    int index2 = i * n + k + p * mNumSamples;
                    Real t = mSamples[index].x;
                    mSamples[index].x = mSamples[index2].x;
                    mSamples[index2].x = t;
                }
            }
        }

        for(int p = 0; p < mNumSets; ++p) {
            for(int i = 0; i < n; ++i) {
                for(int j = 0; j < n; ++j) {
                    int k = RandInt(j, n - 1);
                    int index = j * n + i + p * mNumSamples;
                    int index2 = k * n + i + p * mNumSamples;
                    Real t = mSamples[index].y;
                    mSamples[index].y = mSamples[index2].y;
                    mSamples[index2].y = t;
                }
            }
        }
    }
};
} //end of namespace

#endif
