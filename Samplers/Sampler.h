//
//  Sampler.h
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/14/14.
//
//

#ifndef __RayTracer__Sampler__
#define __RayTracer__Sampler__

#include "../Utilities/Math.h"
#include <vector>
#include <memory>

namespace RayTracer
{

class Sampler
{
public:
    Sampler();
    Sampler(int num);
    Sampler(int num, int numSets);
    virtual ~Sampler() {}

    void SetNumSets(int num) { mNumSets = num; }
    int NumSamples() const { return mNumSamples; }

    virtual void GenerateSamples() = 0;

    void SetupShuffledIndices();

    void ShuffleXCoordinates();
    void ShuffleYCoordinates();

    void MapSamplesToUnitDisk();
    void MapSamplesToHemisphere(Real e);

    Vector2 SampleUnitSquare();
    Vector2 SampleUnitDisk();
    Vector3 SampleHemisphere();

protected:
    int mNumSamples;
    int mNumSets;
    std::vector<Vector2> mSamples;
    std::vector<Vector2> mDiskSamples;
    std::vector<Vector3> mHemisphereSamples;
    std::vector<int> mShuffledIndices;
    unsigned long mCount;
    int mJump;
};

using SamplerPtr = std::unique_ptr<Sampler>;

} //end of namespace

#endif /* defined(__RayTracer__Sampler__) */
