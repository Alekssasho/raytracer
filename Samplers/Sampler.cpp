//
//  Sampler.cpp
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/14/14.
//
//

#include "Sampler.h"
#include <algorithm>
#include <numeric>
#include <iostream>
#include <iterator>

namespace RayTracer
{
Sampler::Sampler()
    : Sampler(1, 83)
{
}
Sampler::Sampler(int num)
    : Sampler(num, 83)
{
}

Sampler::Sampler(int num, int numSets)
    : mNumSamples(num)
    , mNumSets(numSets)
    , mCount(0)
    , mJump(0)
{
    mSamples.reserve(mNumSamples * mNumSets);
    this->SetupShuffledIndices();
}

void Sampler::SetupShuffledIndices()
{
    mShuffledIndices.reserve(mNumSets * mNumSamples);
    std::vector<int> indices(mNumSamples);
    std::iota(indices.begin(), indices.end(), 0);

    for(int p = 0; p < mNumSets; ++p) {
        std::random_shuffle(indices.begin(), indices.end());

        std::copy(indices.begin(), indices.end(), std::back_inserter(mShuffledIndices));
    }
}

void Sampler::MapSamplesToUnitDisk()
{
    int size = mSamples.size();
    Real r, phi;
    Vector2 sp;
    mDiskSamples.reserve(size);

    for(const auto& point : mSamples) {
        sp.x = 2.0f * point.x - 1.0f;
        sp.y = 2.0f * point.y - 1.0f;

        if(sp.x > -sp.y) {
            if(sp.x > sp.y) {
                r = sp.x;
                phi = sp.y / sp.x;
            } else {
                r = sp.y;
                phi = 2 - sp.x / sp.y;
            }
        } else {
            if(sp.x < sp.y) {
                r = -sp.x;
                phi = 4 + sp.y / sp.x;
            } else {
                r = -sp.y;
                if(sp.y != 0.0f)
                    phi = 6 - sp.x / sp.y;
                else
                    phi = 0.0;
            }
        }
        phi *= PI / 4.0f;

        mDiskSamples.emplace_back(r * cos(phi), r * sin(phi));
    }

    mSamples.clear();
}

void Sampler::MapSamplesToHemisphere(Real e)
{
    mHemisphereSamples.reserve(mSamples.size());
    for(const auto& sample : mSamples) {
        Real cosPhi = cosf(2.0f * PI * sample.x);
        Real sinPhi = sinf(2.0f * PI * sample.x);
        Real cosTheta = powf(1.0f - sample.y, 1.0f / (e + 1.0f));
        Real sinTheta = sqrtf(1.0f - cosTheta * cosTheta);
        mHemisphereSamples.emplace_back(sinTheta * cosPhi, sinTheta * sinPhi, cosTheta);
    }

    mSamples.clear();
}

Vector2 Sampler::SampleUnitSquare()
{
    if(mCount % mNumSamples == 0)
        mJump = (RandInt() % mNumSets) * mNumSamples;

    return mSamples[mJump + mShuffledIndices[mJump + (mCount++ % mNumSamples)]];
}

Vector2 Sampler::SampleUnitDisk()
{
    if(mCount % mNumSamples == 0)
        mJump = (RandInt() % mNumSets) * mNumSamples;

    return mDiskSamples[mJump + mShuffledIndices[mJump + (mCount++ % mNumSamples)]];
}

Vector3 Sampler::SampleHemisphere()
{
    if(mCount % mNumSamples == 0)
        mJump = (RandInt() % mNumSets) * mNumSamples;

    return mHemisphereSamples[mJump + mShuffledIndices[mJump + (mCount++ % mNumSamples)]];
}

void Sampler::ShuffleYCoordinates()
{
    for(int p = 0; p < mNumSets; ++p) {
        for(int i = 0; i < mNumSamples - 1; ++i) {
            int target = RandInt() % mNumSamples + p * mNumSamples;
            Real temp = mSamples[i + p * mNumSamples + 1].y;
            mSamples[i + p * mNumSamples + 1].y = mSamples[target].y;
            mSamples[target].y = temp;
        }
    }
}

void Sampler::ShuffleXCoordinates()
{
    for(int p = 0; p < mNumSets; ++p) {
        for(int i = 0; i < mNumSamples - 1; ++i) {
            int target = RandInt() % mNumSamples + p * mNumSamples;
            Real temp = mSamples[i + p * mNumSamples + 1].x;
            mSamples[i + p * mNumSamples + 1].x = mSamples[target].x;
            mSamples[target].x = temp;
        }
    }
}
} //end of namespace