//
//  Jittered.h
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/14/14.
//
//

#ifndef RayTracer_Jittered_h
#define RayTracer_Jittered_h

#include "Sampler.h"

namespace RayTracer
{
class Jittered : public Sampler
{
public:
    Jittered(int num)
        : Sampler(num)
    {
        this->GenerateSamples();
    }

    virtual void GenerateSamples() override
    {
        int n = sqrt(mNumSamples);

        for(int p = 0; p < mNumSets; ++p) {
            for(int j = 0; j < n; ++j) {
                for(int k = 0; k < n; ++k) {
                    mSamples.emplace_back((k + RandReal()) / n, (j + RandReal()) / n);
                }
            }
        }
    }
};
} //end of namespace

#endif
