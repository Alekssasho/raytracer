//
//  Fisheye.cpp
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/17/14.
//
//

#include "Fisheye.h"
#include "../World/World.h"

namespace RayTracer
{
Fisheye::Fisheye()
    : Camera()
    , mPsiMax(90.0f)
{
}

void Fisheye::RenderScene(const World& world)
{
    Color color;
    const ViewPlane& vp = world.GetViewPlane();
    Real size = vp.PixelSize();
    Ray ray(mEye, Vector3());
    Real rSquared;
    int depth = 0;

    for(int r = 0; r < vp.Vres(); ++r) {
        for(int c = 0; c < vp.Hres(); ++c) {
            color = gBlack;

            for(int j = 0; j < vp.NumSamples(); ++j) {
                auto sp = vp.GetSampler().SampleUnitSquare();
                ray.d = this->RayDirection(Vector2(size * (c - 0.5f * vp.Hres() + sp.x),
                                                   size * (r - 0.5f * vp.Vres() + sp.y)),
                                           vp.Hres(),
                                           vp.Vres(),
                                           size,
                                           rSquared);

                if(rSquared <= 1.0f) {
                    color += world.GetTracer().TraceRay(ray, depth);
                }
            }
            color /= vp.NumSamples();
            color *= mExposureTime;
            world.DisplayPixel(r, c, color);
        }
    }
}

Vector3 Fisheye::RayDirection(Vector2 p, int hres, int vres, Real size, Real& rsqr) const
{
    Vector2 pn(2.0f / (size * hres) * p.x,
               2.0f / (size * vres) * p.y);
    rsqr = glm::dot(pn, pn);
    if(rsqr < 1.0f) {
        Real r = glm::sqrt(rsqr);
        Degree psi(r * mPsiMax.ValueDegree());
        Real sinPsi = glm::sin(psi.ValueRadians());
        Real cosPsi = glm::cos(psi.ValueRadians());
        Real sinA = pn.y / r;
        Real cosA = pn.x / r;
        return sinPsi * cosA * mU + sinPsi * sinA * mV - cosPsi * mW;
    }
    return Vector3();
}
} //end of namespace