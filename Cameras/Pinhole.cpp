//
//  Pinhole.cpp
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/15/14.
//
//

#include "Pinhole.h"
#include "../World/World.h"

namespace RayTracer
{
Pinhole::Pinhole()
    : Camera()
    , mDistance(500.0f)
    , mZoom(1.0f)
{
}

Vector3 Pinhole::RayDirection(const Vector2& p) const
{
    using namespace glm;
    return normalize(p.x * mU + p.y * mV - mDistance * mW);
}

void Pinhole::RenderScene(const World& world)
{
    Color color;
    Ray ray(mEye, Vector3(0.0f));
    const ViewPlane& vp = world.GetViewPlane();
    Real size = vp.PixelSize() / mZoom;
    Vector2 sp;
    int depth = 0;

    for(int r = 0; r < vp.Vres(); ++r) {
        for(int c = 0; c < vp.Hres(); ++c) {
            color = gBlack;

            for(int j = 0; j < vp.NumSamples(); ++j) {
                sp = vp.GetSampler().SampleUnitSquare();
                ray.d = this->RayDirection(Vector2(size * (c - 0.5f * vp.Hres() + sp.x),
                                                   size * (r - 0.5f * vp.Vres() + sp.y)));
                color += world.GetTracer().TraceRay(ray, depth);
            }

            color /= vp.NumSamples();
            color *= mExposureTime;
            world.DisplayPixel(r, c, color);
        }
    }
}

} //end of namespace