//
//  ThinLens.cpp
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/15/14.
//
//

#include "ThinLens.h"
#include "../World/World.h"

namespace RayTracer
{

ThinLens::ThinLens()
    : Camera()
    , mLensRadius(1.0f)
    , mDistance(0.0f)
    , mFocalDistance(0.0f)
    , mZoom(1.0f)
{
}

void ThinLens::SetSampler(Sampler* samp)
{
    mpSampler.reset(samp);
    mpSampler->MapSamplesToUnitDisk();
}

Vector3 ThinLens::RayDirection(Vector2 pixelPoint, Vector2 lensPoint) const
{
    Vector2 p(pixelPoint.x * mFocalDistance / mDistance, pixelPoint.y * mFocalDistance / mDistance); //focal plane hit point
    return glm::normalize((p.x - lensPoint.x) * mU + (p.y - lensPoint.y) * mV - mFocalDistance * mW);
}

void ThinLens::RenderScene(const World& world)
{
    Color color;
    Ray ray;
    const ViewPlane& vp = world.GetViewPlane();
    Real size = vp.PixelSize() / mZoom;
    int depth = 0;

    for(int r = 0; r < vp.Vres(); ++r) {
        for(int c = 0; c < vp.Hres(); ++c) {
            color = gBlack;

            for(int n = 0; n < vp.NumSamples(); ++n) {
                auto sp = vp.GetSampler().SampleUnitSquare();
                Vector2 pp(size * (c - vp.Hres() / 2.0f + sp.x),
                           size * (r - vp.Vres() / 2.0f + sp.y));

                auto dp = mpSampler->SampleUnitDisk();
                auto lp = dp * mLensRadius;

                ray.o = mEye + lp.x * mU + lp.y * mV;
                ray.d = this->RayDirection(pp, lp);
                color += world.GetTracer().TraceRay(ray, depth);
            }

            color /= vp.NumSamples();
            color *= mExposureTime;
            world.DisplayPixel(r, c, color);
        }
    }
}
} //end of namespace