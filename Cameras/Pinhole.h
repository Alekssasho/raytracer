//
//  Pinhole.h
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/15/14.
//
//

#ifndef __RayTracer__Pinhole__
#define __RayTracer__Pinhole__

#include "Camera.h"

namespace RayTracer
{
class Pinhole : public Camera
{
public:
    Pinhole();

    void SetDistance(Real d) { mDistance = d; }
    void SetZoom(Real zoom) { mZoom = zoom; }

    Vector3 RayDirection(const Vector2& p) const;
    virtual void RenderScene(const World& world) override;

private:
    Real mDistance;
    Real mZoom;
};
} //end of namespace

#endif /* defined(__RayTracer__Pinhole__) */
