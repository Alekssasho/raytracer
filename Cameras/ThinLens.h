//
//  ThinLens.h
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/15/14.
//
//

#ifndef __RayTracer__ThinLens__
#define __RayTracer__ThinLens__

#include "Camera.h"
#include "../Samplers/Sampler.h"

namespace RayTracer
{
class ThinLens : public Camera
{
public:
    ThinLens();

    virtual void RenderScene(const World& world) override;

    //Take ownership of the sampelr
    void SetSampler(Sampler* pSamp);

    void SetLensRadius(Real rad) { mLensRadius = rad; }
    void SetDistance(Real dist) { mDistance = dist; }
    void SetFocalDistance(Real dist) { mFocalDistance = dist; }
    void SetZoom(Real zoom) { mZoom = zoom; }

    Vector3 RayDirection(Vector2 pixelPoint, Vector2 lensPoint) const;

private:
    Real mLensRadius;
    Real mDistance;
    Real mFocalDistance;
    Real mZoom;
    SamplerPtr mpSampler;
};
} //end of namespace
#endif /* defined(__RayTracer__ThinLens__) */
