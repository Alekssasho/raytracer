//
//  Camera.h
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/15/14.
//
//

#ifndef RayTracer_Camera_h
#define RayTracer_Camera_h

#include "../Utilities/Math.h"
#include <glm/gtx/transform.hpp>

namespace RayTracer
{

class World;

class Camera
{
public:
    Camera()
        : mEye(0.0f, 0.0f, 500.0f)
        , mLookAt(0.0f)
        , mUp(0.0f, 1.0f, 0.0f)
        , mU(1.0f, 0.0f, 0.0f)
        , mV(0.0f, 1.0f, 0.0f)
        , mW(0.0f, 0.0f, 1.0f)
        , mRoll(0.0f)
        , mExposureTime(1.0f)
    {
    }

    virtual ~Camera() {}

    void ComputeUVW();
    virtual void RenderScene(const World& world) = 0;

    void SetEye(Vector3 eye) { mEye = eye; }
    void SetLookAt(Vector3 lookat) { mLookAt = lookat; }
    void SetUp(Vector3 up) { mUp = up; }
    void SetExposure(Real exp) { mExposureTime = exp; }
    void SetRollAngle(Degree roll) { mRoll = roll; }

protected:
    Vector3 mEye;
    Vector3 mLookAt;
    Vector3 mUp;
    Vector3 mU, mV, mW;
    Degree mRoll;
    Real mExposureTime;
};

inline void Camera::ComputeUVW()
{
    using namespace glm;
    mW = normalize(mEye - mLookAt);
    mU = normalize(cross(mUp, mW));
    mV = cross(mW, mU);

    if(mEye.xz() == mLookAt.xz()) {
        if(mEye.y > mLookAt.y) { // camera looking vertically down
            mU = Vector3(0.0f, 0.0f, 1.0f);
            mV = Vector3(1.0f, 0.0f, 0.0f);
            mW = Vector3(0.0f, 1.0f, 0.0f);
        } else if(mEye.y < mLookAt.y) { // camera looking vertically up
            mU = Vector3(1.0f, 0.0f, 0.0f);
            mV = Vector3(0.0f, 0.0f, 1.0f);
            mW = Vector3(0.0f, -1.0f, 0.0f);
        }
    }

    //Rotate with roll angle
    if(mRoll.ValueDegree() != 0.0f) {
        auto rotateMatrix = rotate(mRoll.ValueRadians(), -mW);
        mU = (rotateMatrix * vec4(mU, 1.0f)).xyz();
        mV = (rotateMatrix * vec4(mV, 1.0f)).xyz();
    }
}
} //end of namespace

#endif
