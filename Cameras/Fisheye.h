//
//  Fisheye.h
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/17/14.
//
//

#ifndef __RayTracer__Fisheye__
#define __RayTracer__Fisheye__

#include "Camera.h"

namespace RayTracer
{
class Fisheye : public Camera
{
public:
    Fisheye();

    Vector3 RayDirection(Vector2 p, int hres, int vres, Real size, Real& r) const;

    virtual void RenderScene(const World& world) override;

    void SetFieldOfView(Degree view) { mPsiMax = Degree(view.ValueDegree() / 2.0f); }

private:
    Degree mPsiMax;
};
} //end of namespace

#endif /* defined(__RayTracer__Fisheye__) */
