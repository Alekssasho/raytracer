//
//  World.h
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/10/14.
//
//

#ifndef __RayTracer__World__
#define __RayTracer__World__

#include "../Utilities/Math.h"
#include "../Tracers/Tracer.h"
#include "../GeometricObjects/GeometricObject.h"
#include "../Cameras/Camera.h"
#include "../Lights/Light.h"
#include "ViewPlane.h"
#include <memory>
#include <functional>
#include <vector>

extern const int kScreenWidth;
extern const int kScreenHeight;

namespace RayTracer
{

using TracerPtr = std::unique_ptr<Tracer>;
using CameraPtr = std::unique_ptr<Camera>;
using PixelDisplayFunc = std::function<void(int, int, const Color&)>;
using LightPtr = std::unique_ptr<Light>;

class World
{
public:
    World(PixelDisplayFunc func);

    void Build();
    void RenderScene() const;
    void DisplayPixel(int row, int column, const Color& pixelColod) const;

    Color BackgroundColor() const { return mBackgroundColor; }
    void SetBackgroundColor(const Color& c) { mBackgroundColor = c; }

    void AddObject(GeometricObject* pObj) { mObjects.push_back(GeomObjectPtr(pObj)); }
    void AddLight(Light* pLight) { mLights.push_back(LightPtr(pLight)); }

    ShadeRec HitObjects(const Ray& ray);

    const ViewPlane& GetViewPlane() const { return mViewPlane; }
    const Tracer& GetTracer() const { return *mpTracer.get(); }

    //    void SetAmbientLight(Light* pLight) { mpAmbient.reset(pLight); }
    Light& GetAmbientLight() const { return *mpAmbient.get(); }

    const std::vector<LightPtr>& GetLights() const { return mLights; }
    const std::vector<GeomObjectPtr>& GetObjects() const { return mObjects; }

private:
    ViewPlane mViewPlane;
    Color mBackgroundColor;
    TracerPtr mpTracer;
    CameraPtr mpCamera;
    LightPtr mpAmbient;
    PixelDisplayFunc mPixelDisplayFunc;

    std::vector<GeomObjectPtr> mObjects;
    std::vector<LightPtr> mLights;
};
} // end of namespace

#endif /* defined(__RayTracer__World__) */
