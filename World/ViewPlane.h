#ifndef RAYTRACER_VIEW_PLANE_H
#define RAYTRACER_VIEW_PLANE_H

#include "../Utilities/Math.h"
#include "../Samplers/Sampler.h"

namespace RayTracer
{
class ViewPlane
{
private:
    int mHres;
    int mVres;
    Real mPixelSize; //pixel size
    Real mGamma;
    Real mInvGamma;
    SamplerPtr mpSampler;
    int mMaxDepth;

public:
    ViewPlane();

    int Hres() const { return mHres; }
    int Vres() const { return mVres; }
    Real PixelSize() const { return mPixelSize; }
    Real Gamma() const { return mGamma; }
    Real InvGamma() const { return mInvGamma; }
    int MaxDepth() const { return mMaxDepth; }

    void SetHres(int hres) { mHres = hres; }
    void SetVres(int vres) { mVres = vres; }
    void SetPixelSize(Real size) { mPixelSize = size; }
    void setMaxDepth(int depth) { mMaxDepth = depth; }
    void SetGamma(Real gamma)
    {
        mGamma = gamma;
        mInvGamma = 1.0f / gamma;
    }

    void SetNumSamples(int num);
    void SetSampler(Sampler* pSampler);
    int NumSamples() const { return mpSampler->NumSamples(); }

    Sampler& GetSampler() const { return *mpSampler.get(); }
};
} //end of namespace

#endif