#include "ViewPlane.h"
#include "../Samplers/Regular.h"
#include "../Samplers/MultiJittered.h"

namespace RayTracer
{
ViewPlane::ViewPlane()
    : mHres(400)
    , mVres(400)
    , mPixelSize(1.0f)
    , mGamma(1.0f)
    , mInvGamma(1.0f)
    , mMaxDepth(1)
{
}

void ViewPlane::SetSampler(Sampler* pSampler)
{
    mpSampler.reset(pSampler);
}

void ViewPlane::SetNumSamples(int num)
{
    if(num > 1)
        mpSampler.reset(new MultiJittered(num));
    else
        mpSampler.reset(new Regular(1));
}
} //end of namespace