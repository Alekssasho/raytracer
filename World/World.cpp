//
//  World.cpp
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/10/14.
//
//

#include "World.h"

#include <glm/ext.hpp>
#include "../Lights/Ambient.h"

namespace RayTracer
{

World::World(PixelDisplayFunc func)
    : mBackgroundColor(gBlack)
    , mpAmbient(new Ambient)
    , mPixelDisplayFunc(func)
{
}

void World::RenderScene() const
{
    mpCamera->RenderScene(*this);
}

static Color MaxToOne(const Color& c)
{
    using glm::max;
    Real maxValue = max(c.r, max(c.g, c.b));

    if(maxValue > 1.0f)
        return c / maxValue;
    else
        return c;
}

void World::DisplayPixel(int row, int column, const Color& c) const
{
    Color color = MaxToOne(c);

    if(mViewPlane.Gamma() != 1.0f) {
        color.r = glm::pow(color.r, mViewPlane.InvGamma());
        color.g = glm::pow(color.g, mViewPlane.InvGamma());
        color.b = glm::pow(color.b, mViewPlane.InvGamma());
    }

    int y = mViewPlane.Vres() - row - 1;
    mPixelDisplayFunc(column, y, Color(color * 255.0f));
}

ShadeRec World::HitObjects(const Ray& ray)
{
    ShadeRec sr(*this);
    Real t;
    Real tmin = std::numeric_limits<Real>::max();
    Vector3 normal;
    Vector3 localHitPoint;

    for(const auto& pObj : mObjects) {
        if(pObj->Hit(ray, t, sr) && (t < tmin)) {
            sr.hitObject = true;
            tmin = t;
            sr.pMat = pObj->GetMaterial();
            sr.hitPoint = ray.o + t * ray.d;
            normal = sr.normal;
            localHitPoint = sr.localHitPoint;
        }
    }

    if(sr.hitObject) {
        sr.t = tmin;
        sr.normal = normal;
        sr.localHitPoint = localHitPoint;
    }

    return sr;
}
} //end of namespace

#include "../build/BuildFunction.cpp"