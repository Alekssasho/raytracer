//
//  Math.h
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/9/14.
//
//

#ifndef RayTracer_Math_h
#define RayTracer_Math_h

#define GLM_FORCE_RADIANS
#define GLM_SWIZZLE
#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>

namespace RayTracer
{

using Vector2 = glm::vec2;
using Vector3 = glm::vec3;
using Vector4 = glm::vec4;
using Matrix4 = glm::mat4;
using Color = glm::vec4;
using Real = float;

const Matrix4 gIdentity(1.0f);

const Vector3 gUp(0.0f, 1.0f, 0.0f);
const Vector3 gRight(1.0f, 0.0f, 0.0f);
const Vector3 gForward(0.0f, 0.0f, -1.0f); // -1 because of Right Handed System used in the engine

constexpr Real OPAQUE = 1.0f;
constexpr Real TRANSPARENT = 0.0f;

const Color gWhite(1.0f, 1.0f, 1.0f, OPAQUE);
const Color gBlack(0.0f, 0.0f, 0.0f, OPAQUE);
const Color gCyan(0.0f, 1.0f, 1.0f, OPAQUE);
const Color gRed(1.0f, 0.0f, 0.0f, OPAQUE);
const Color gGreen(0.0f, 1.0f, 0.0f, OPAQUE);
const Color gBlue(0.0f, 0.0f, 1.0f, OPAQUE);
const Color gYellow(1.0f, 1.0f, 0.0f, OPAQUE);
const Color gGray40(0.4f, 0.4f, 0.4f, OPAQUE);
const Color gGray65(0.65f, 0.65f, 0.65f, OPAQUE);
const Color gGray25(0.25f, 0.25f, 0.25f, OPAQUE);
const Color gTransparent(1.0f, 0.0f, 1.0f, TRANSPARENT);

const Real PI = glm::pi<Real>();
const Real TWO_PI = 2 * PI;
const Real INV_PI = glm::one_over_pi<Real>();
const Real kEpsilon = 0.0001f;

inline int RandInt()
{
    return rand();
}

inline Real RandReal()
{
    return static_cast<Real>(rand()) / static_cast<Real>(RAND_MAX);
}

struct Ray
{
    Vector3 o; //origin
    Vector3 d; //direction

    Ray()
        : o(0.0f)
        , d(0.0f, 0.0f, 0.1f)
    {
    }

    Ray(const Vector3& origin, const Vector3& direction)
        : o(origin)
        , d(direction)
    {
    }
};

class World;
class Material;
struct ShadeRec
{
    bool hitObject;
    Material* pMat;
    Vector3 hitPoint; //world coordinates
    Vector3 localHitPoint;
    Vector3 normal;
    Ray ray; //for specular highlights
    int depth; //recursion depth
    Vector3 dir; //for area lights
    Real t; // ray paramater
    World& world;

    ShadeRec(World& wr)
        : hitObject(false)
        , pMat(nullptr)
        , depth(0)
        , world(wr)
    {
    }
};

class Radian;
class Degree
{
public:
    explicit Degree(Real value = 0.0f)
        : mValue(value)
    {
    }

    inline Degree(const Radian& rad);
    Real ValueDegree() const { return mValue; }
    Real ValueRadians() const { return glm::radians(mValue); }

    //TODO: implement all kind of math operations

private:
    Real mValue;
};

class Radian
{
public:
    explicit Radian(Real value = 0.0f)
        : mValue(value)
    {
    }

    Radian(const Degree& deg)
        : mValue(deg.ValueRadians())
    {
    }

    Real ValueDegree() const { return glm::degrees(mValue); }
    Real ValueRadians() const { return mValue; }

private:
    Real mValue;
};

inline Degree::Degree(const Radian& rad)
    : mValue(rad.ValueDegree())
{
}

} //end of namespace RayTracer

#endif
