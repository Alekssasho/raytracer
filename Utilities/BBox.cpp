//
//  BBox.cpp
//  RayTracer
//
//  Created by Aleksandar Angelov on 8/5/14.
//
//

#include "BBox.h"
#include <cmath>

namespace RayTracer
{
    BBox::BBox()
    :BBox(0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f)
    {
    }
    
    BBox::BBox(Real _x0, Real _x1, Real _y0, Real _y1, Real _z0, Real _z1)
    : x0(_x0), x1(_x1), y0(_y0), y1(_y1), z0(_z0), z1(_z1)
    {
    }
    
    BBox::BBox(Vector3 p0, Vector3 p1)
    :BBox(p0.x, p0.y, p0.z, p1.x, p1.y, p1.z)
    {
    }
    
    bool BBox::Hit(const Ray& ray) const
    {
        Real ox = ray.o.x, oy = ray.o.y, oz = ray.o.z;
        Real dx = ray.d.x, dy = ray.d.y, dz = ray.d.z;
        
        Real tx_min, ty_min, tz_min, tx_max, ty_max, tz_max;
        
        Real a = 1.0f / dx;
        if (a >= 0) {
            tx_min = (x0 - ox) * a;
            tx_max = (x1 - ox) * a;
        } else {
            tx_min = (x1 - ox) * a;
            tx_max = (x0 - ox) * a;
        }
        
        Real b = 1.0f / dy;
        if (b >= 0) {
            ty_min = (y0 - oy) * b;
            ty_max = (y1 - oy) * b;
        } else {
            ty_min = (y1 - oy) * b;
            ty_max = (y0 - oy) * b;
        }
        
        Real c = 1.0f / dz;
        if (c >= 0) {
            tz_min = (z0 - oz) * c;
            tz_max = (z1 - oz) * c;
        } else {
            tz_min = (z1 - oz) * c;
            tz_max = (z0 - oz) * c;
        }
        
        Real t0 = fmax(fmax(tx_min, ty_min), tz_min);
        
        Real t1 = fmin(fmin(tx_max, ty_max), tz_max);
        
        return t0 < t1 && t1 > kEpsilon;
    }
    
    bool BBox::Inside(const Vector3& p) const
    {
        return (p.x > x0 && p.x < x1) && (p.y > y0 && p.y < y1) && (p.z > z0 && p.z < z1);
    }
}//end of namespace
