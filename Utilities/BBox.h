//
//  BBox.h
//  RayTracer
//
//  Created by Aleksandar Angelov on 8/5/14.
//
//

#ifndef __RayTracer__BBox__
#define __RayTracer__BBox__

#include "Math.h"

namespace RayTracer
{
    
    class BBox
    {
    public:
        Real x0, x1, y0, y1, z0, z1;
        
        BBox();
        BBox(Real x0, Real x1, Real y0, Real y1, Real z0, Real z1);
        BBox(Vector3 p0, Vector3 p1);
        
        bool Hit(const Ray& ray) const;
        bool Inside(const Vector3& p) const;
    };
    
}//end of namespace

#endif /* defined(__RayTracer__BBox__) */
