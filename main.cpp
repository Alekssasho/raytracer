#include <SDL2/SDL.h>
#include <iostream>
#include <thread>
#include <string>
#include <chrono>
#include "Utilities/Math.h"
#include "World/World.h"

using namespace RayTracer;

extern const int kScreenWidth;
extern const int kScreenHeight;

SDL_Window* gWindow;
SDL_Renderer* gRenderer;

bool gRunning;

void mainLoop();
void handleInput();
void renderScreen();
void rayTrace();
void renderPixel(int x, int y, const Color& color);

void saveImage();

int main(int argc, char* argv[])
{
    SDL_Init(SDL_INIT_VIDEO);

    gWindow = SDL_CreateWindow("Ray Tracer", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, kScreenWidth, kScreenHeight, 0);

    gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_SOFTWARE);
    std::cerr << SDL_GetError();

    mainLoop();

    SDL_DestroyRenderer(gRenderer);
    SDL_DestroyWindow(gWindow);

    SDL_Quit();

    return 0;
}

void mainLoop()
{
    gRunning = true;

    std::thread renderThread(rayTrace);

    while(gRunning) {
        handleInput();
        renderScreen();
    }

    renderThread.join();
}

void renderPixel(int x, int y, const Color& color)
{
    SDL_SetRenderDrawColor(gRenderer, color.r, color.g, color.b, 255);
    SDL_RenderDrawPoint(gRenderer, x, y);
}

void rayTrace()
{
    using namespace std;
    using namespace std::chrono;

    cout << "Starting building scene\n";
    auto start = system_clock::now();

    World world(renderPixel);
    world.Build();
    cout << "Finished building scene\nStarting ray tracing\n";
    world.RenderScene();
    cout << "Finished ray tracing\n";

    auto finish = system_clock::now();
    cout << "Rendering took ";

    auto time = finish - start;

    auto hoursTook = duration_cast<hours>(time).count();
    if(hoursTook)
        cout << hoursTook << "h ";

    auto minutesTook = duration_cast<minutes>(time).count() - hoursTook * 60;
    if(minutesTook)
        cout << minutesTook << "m ";

    auto secondsTook = duration_cast<seconds>(time).count() - (minutesTook + hoursTook * 60) * 60;
    if(secondsTook)
        cout << secondsTook << "s ";

    auto milisecondsTook = duration_cast<milliseconds>(time).count() - (secondsTook + minutesTook * 60 + hoursTook * 60 * 60) * 1000;
    if(milisecondsTook)
        cout << milisecondsTook << "ms ";

    cout << endl;
}

void handleInput()
{
    SDL_Event e;
    while(SDL_PollEvent(&e)) {
        if(e.type == SDL_QUIT) {
            gRunning = false;
            return;
        } else if(e.type == SDL_KEYDOWN) {
            switch(e.key.keysym.sym) {
            case SDLK_ESCAPE:
                gRunning = false;
                return;
                break;
            case SDLK_s:
                saveImage();
                break;
            default:
                break;
            }
        }
    }
}

void renderScreen()
{
    //SDL_RenderClear(gRenderer);
    SDL_RenderPresent(gRenderer);
}

void saveImage()
{
    static int count = 0;
    std::cout << "Image saving started\n";
    SDL_Surface* surface = SDL_CreateRGBSurface(0,
                                                kScreenWidth,
                                                kScreenHeight,
                                                32,
                                                0xff000000,
                                                0x00ff0000,
                                                0x0000ff00,
                                                0x000000ff);
    SDL_Rect rect{ 0, 0, kScreenWidth, kScreenHeight };
    SDL_RenderReadPixels(gRenderer,
                         &rect,
                         SDL_PIXELFORMAT_RGBA8888,
                         surface->pixels,
                         surface->pitch);
    std::string filename = "image" + std::to_string(count++) + ".bmp";
    SDL_SaveBMP(surface, filename.c_str());
    SDL_FreeSurface(surface);
    std::cout << "Image saved sucessfuly\n";
}
