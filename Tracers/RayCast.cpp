//
//  RayCast.cpp
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/22/14.
//
//

#include "RayCast.h"
#include "../World/World.h"
#include "../Materials/Material.h"

namespace RayTracer
{
Color RayCast::TraceRay(const Ray ray, int depth) const
{
    ShadeRec sr = mWorld.HitObjects(ray);

    if(sr.hitObject) {
        sr.ray = ray;
        return sr.pMat->Shade(sr);
    }
    return mWorld.BackgroundColor();
}
} //end of namespace