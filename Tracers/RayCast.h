//
//  RayCast.h
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/22/14.
//
//

#ifndef __RayTracer__RayCast__
#define __RayTracer__RayCast__

#include "Tracer.h"

namespace RayTracer
{
class RayCast : public Tracer
{
public:
    RayCast(World& world)
        : Tracer(world)
    {
    }

    Color TraceRay(const Ray ray, int depth) const override;
};
}

#endif /* defined(__RayTracer__RayCast__) */
