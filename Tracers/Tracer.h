#ifndef RAYTRACER_TRACER_H
#define RAYTRACER_TRACER_H

#include "../Utilities/Math.h"

namespace RayTracer
{
class Tracer
{
public:
    Tracer(World& world)
        :mWorld(world)
    {
    }


    virtual ~Tracer() {}

    virtual Color TraceRay(const Ray& ray) const
    {
        return gBlack;
    }
    
    virtual Color TraceRay(const Ray ray, int depth) const
    {
        return gBlack;
    }

protected:
    World& mWorld;
};
}//end of namespace

#endif