//
//  AreaLighting.h
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/25/14.
//
//

#ifndef __RayTracer__AreaLighting__
#define __RayTracer__AreaLighting__

#include "Tracer.h"

namespace RayTracer
{
class AreaLighting : public Tracer
{
public:
    AreaLighting(World& world)
        : Tracer(world)
    {
    }

    Color TraceRay(const Ray ray, int depth) const override;
};
} //end of namespace

#endif /* defined(__RayTracer__AreaLighting__) */
