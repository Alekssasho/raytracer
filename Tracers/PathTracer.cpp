//
//  PathTracer.cpp
//  RayTracer
//
//  Created by Aleksandar Angelov on 8/27/14.
//
//

#include "PathTracer.h"
#include "../World/World.h"

namespace RayTracer
{
    
    Color PathTracer::TraceRay(const Ray ray, int depth) const
    {
        if(depth > mWorld.GetViewPlane().MaxDepth())
            return gBlack;
        
        ShadeRec sr(mWorld.HitObjects(ray));
        
        if(sr.hitObject) {
            sr.depth = depth;
            sr.ray = ray;
            
            return sr.pMat->PathShade(sr);
        }
        
        return mWorld.BackgroundColor();
    }

}