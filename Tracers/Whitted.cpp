//
//  Whitted.cpp
//  RayTracer
//
//  Created by Aleksandar Angelov on 8/22/14.
//
//

#include "Whitted.h"
#include "../World/World.h"

namespace RayTracer
{
    
    Color Whitted::TraceRay(const Ray ray, int depth) const
    {
        if(depth > mWorld.GetViewPlane().MaxDepth())
            return gBlack;
        
        ShadeRec sr(mWorld.HitObjects(ray));
        
        if(sr.hitObject) {
            sr.depth = depth;
            sr.ray = ray;
            
            return sr.pMat->Shade(sr);
        } else {
            return mWorld.BackgroundColor();
        }
    }
    
}//end of namespace