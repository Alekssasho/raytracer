//
//  Whitted.h
//  RayTracer
//
//  Created by Aleksandar Angelov on 8/22/14.
//
//

#ifndef __RayTracer__Whitted__
#define __RayTracer__Whitted__

#include "Tracer.h"

namespace RayTracer
{
    class Whitted : public Tracer
    {
    public:
        Whitted(World& world)
        : Tracer(world)
        {
        }
        
        Color TraceRay(const Ray ray, int depth) const override;
    };
}//end of namespace

#endif /* defined(__RayTracer__Whitted__) */
