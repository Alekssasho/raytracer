//
//  PathTracer.h
//  RayTracer
//
//  Created by Aleksandar Angelov on 8/27/14.
//
//

#ifndef __RayTracer__PathTracer__
#define __RayTracer__PathTracer__

#include "Tracer.h"

namespace RayTracer
{
    
    class PathTracer : public Tracer
    {
    public:
        PathTracer(World& world)
        :Tracer(world)
        {}
        
        Color TraceRay(const Ray ray, int depth) const override;
    };
    
}//end of namespace

#endif /* defined(__RayTracer__PathTracer__) */
