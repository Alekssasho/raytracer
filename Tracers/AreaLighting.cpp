//
//  AreaLighting.cpp
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/25/14.
//
//

#include "AreaLighting.h"
#include "../World/World.h"

namespace RayTracer
{
Color AreaLighting::TraceRay(const Ray ray, int depth) const
{
    ShadeRec sr = mWorld.HitObjects(ray);

    if(sr.hitObject) {
        sr.ray = ray;
        sr.depth = depth;
        return sr.pMat->AreaLightShade(sr);
    }

    return mWorld.BackgroundColor();
}
} //end of namespace