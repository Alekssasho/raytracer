#include "../World/World.h"
#include "../Tracers/RayCast.h"
#include "../Tracers/AreaLighting.h"
#include "../Tracers/Whitted.h"
#include "../Tracers/PathTracer.h"
#include "../Cameras/Pinhole.h"
#include "../Cameras/ThinLens.h"
#include "../Cameras/Fisheye.h"
#include "../Lights/PointLight.h"
#include "../Lights/Directional.h"
#include "../Lights/AreaLight.h"
#include "../Lights/EnvironmentLight.h"
#include "../Materials/Matte.h"
#include "../Materials/Phong.h"
#include "../Materials/Emissive.h"
#include "../Materials/Reflective.h"
#include "../Materials/GlossyReflector.h"

#include "../GeometricObjects/Sphere.h"
#include "../GeometricObjects/Plane.h"
#include "../GeometricObjects/Rectangle.h"
#include "../Samplers/MultiJittered.h"
#include "../Samplers/Jittered.h"
#include "../Samplers/Regular.h"

#include "../Lights/AmbientOccluder.h"

#ifdef __APPLE__
template <typename T, typename... Args>
std::unique_ptr<T> make_unique(Args&&... args)
{
    return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}
#else
using namespace std;
#endif

const int kScreenWidth = 300;
const int kScreenHeight = 300;

void RayTracer::World::Build()
{
    int numSamples = 1024;
    mViewPlane.SetHres(kScreenWidth);
    mViewPlane.SetVres(kScreenHeight);
    mViewPlane.SetNumSamples(numSamples);
    //    auto ps = new Regular(256);
    //    mViewPlane.SetSampler(ps);
    mViewPlane.SetPixelSize(1);
    mViewPlane.setMaxDepth(10);

//    mBackgroundColor = gBlue;

    mpTracer.reset(new PathTracer(*this));

//    auto pSampler = new MultiJittered(numSamples);
//
//    auto ambient = new AmbientOccluder;
//    ambient->ScaleRadiance(1.0f);
//    ambient->SetMinAmount(0.5f);
//    ambient->SetSampler(pSampler);
//    //    mpAmbient.reset(ambient);

    auto pCamera = new Pinhole();
    pCamera->SetEye(Vector3(27.6, 27.4, -80.0));
    pCamera->SetLookAt(Vector3(27.6, 27.4, 0.0));
    pCamera->SetDistance(400);
    pCamera->ComputeUVW();
    mpCamera.reset(pCamera);
    
    Vector3 p0;
	Vector3 a, b;
	Vector3 normal;
	
	// box dimensions
	
	double width 	= 55.28;   	// x direction
	double height 	= 54.88;  	// y direction
	double depth 	= 55.92;	// z direction
	
	
	// the ceiling light - doesn't need samples
	
	Emissive* emissive_ptr = new Emissive;
	emissive_ptr->SetColor(Color(1.0, 0.73, 0.4, OPAQUE));
	emissive_ptr->ScaleRadiance(100);
	
	p0 = Vector3(21.3, height - 0.001, 22.7);
	a = Vector3(0.0, 0.0, 10.5);
	b = Vector3(13.0, 0.0, 0.0);
	normal = Vector3(0.0, -1.0, 0.0);
	Rectangle* light_ptr = new Rectangle(p0, a, b, normal);
	light_ptr->SetMaterial(emissive_ptr);
	AddObject(light_ptr);
	
    
	// left wall
	
	Matte* matte_ptr1 = new Matte;
	matte_ptr1->SetKa(0.0);
	matte_ptr1->SetKd(0.6);
	matte_ptr1->SetColor(Color(0.57, 0.025, 0.025, OPAQUE));	 // red
	matte_ptr1->SetSampler(new MultiJittered(numSamples));
	
	p0 = Vector3(width, 0.0, 0.0);
	a = Vector3(0.0, 0.0, depth);
	b = Vector3(0.0, height, 0.0);
	normal = Vector3(-1.0, 0.0, 0.0);
	Rectangle* left_wall_ptr = new Rectangle(p0, a, b, normal);
	left_wall_ptr->SetMaterial(matte_ptr1);
	AddObject(left_wall_ptr);
	
	
	// right wall
	
	Matte* matte_ptr2 = new Matte;
	matte_ptr2->SetKa(0.0);
	matte_ptr2->SetKd(0.6);
	matte_ptr2->SetColor(Color(0.37, 0.59, 0.2, OPAQUE));	 // green   from Photoshop
	matte_ptr2->SetSampler(new MultiJittered(numSamples));
	
	p0 = Vector3(0.0, 0.0, 0.0);
	a = Vector3(0.0, 0.0, depth);
	b = Vector3(0.0, height, 0.0);
	normal = Vector3(1.0, 0.0, 0.0);
	Rectangle* right_wall_ptr = new Rectangle(p0, a, b, normal);
	right_wall_ptr->SetMaterial(matte_ptr2);
	AddObject(right_wall_ptr);
	
	
	// back wall
	
	Matte* matte_ptr3 = new Matte;
	matte_ptr3->SetKa(0.0);
	matte_ptr3->SetKd(0.6);
	matte_ptr3->SetColor(gWhite);	 // white
	matte_ptr3->SetSampler(new MultiJittered(numSamples));
	
	p0 = Vector3(0.0, 0.0, depth);
	a = Vector3(width, 0.0, 0.0);
	b = Vector3(0.0, height, 0.0);
	normal = Vector3(0.0, 0.0, -1.0);
	Rectangle* back_wall_ptr = new Rectangle(p0, a, b, normal);
	back_wall_ptr->SetMaterial(matte_ptr3);
	AddObject(back_wall_ptr);
	
	
	// floor
    
    Matte* matte_ptr4 = new Matte;
	matte_ptr4->SetKa(0.0);
	matte_ptr4->SetKd(0.6);
	matte_ptr4->SetColor(gWhite);	 // white
	matte_ptr4->SetSampler(new MultiJittered(numSamples));
	
	p0 = Vector3(0.0, 0.0, 0.0);
	a = Vector3(0.0, 0.0, depth);
	b = Vector3(width, 0.0, 0.0);
	normal = Vector3(0.0, 1.0, 0.0);
	Rectangle* floor_ptr = new Rectangle(p0, a, b, normal);
	floor_ptr->SetMaterial(matte_ptr4);
	AddObject(floor_ptr);
	
	
	// ceiling
    
    Matte* matte_ptr5 = new Matte;
	matte_ptr5->SetKa(0.0);
	matte_ptr5->SetKd(0.6);
	matte_ptr5->SetColor(gWhite);	 // white
	matte_ptr5->SetSampler(new MultiJittered(numSamples));
	
	p0 = Vector3(0.0, height, 0.0);
	a = Vector3(0.0, 0.0, depth);
	b = Vector3(width, 0.0, 0.0);
	normal = Vector3(0.0, -1.0, 0.0);
	Rectangle* ceiling_ptr = new Rectangle(p0, a, b, normal);
	ceiling_ptr->SetMaterial(matte_ptr5);
	AddObject(ceiling_ptr);
    
	
	// the two boxes defined as 5 rectangles each
	
	// short box
	
	// top
    
    Matte* matte_ptr6 = new Matte;
	matte_ptr6->SetKa(0.0);
	matte_ptr6->SetKd(0.6);
	matte_ptr6->SetColor(gWhite);	 // white
	matte_ptr6->SetSampler(new MultiJittered(numSamples));
	
	p0 = Vector3(13.0, 16.5, 6.5);
	a = Vector3(-4.8, 0.0, 16.0);
	b = Vector3(16.0, 0.0, 4.9);
	normal = Vector3(0.0, 1.0, 0.0);
	Rectangle* short_top_ptr = new Rectangle(p0, a, b, normal);
	short_top_ptr->SetMaterial(matte_ptr6);
	AddObject(short_top_ptr);
	
	
	// side 1
    
    Matte* matte_ptr7 = new Matte;
	matte_ptr7->SetKa(0.0);
	matte_ptr7->SetKd(0.6);
	matte_ptr7->SetColor(gWhite);	 // white
	matte_ptr7->SetSampler(new MultiJittered(numSamples));
	
	p0 = Vector3(13.0, 0.0, 6.5);
	a = Vector3(-4.8, 0.0, 16.0);
	b = Vector3(0.0, 16.5, 0.0);
	Rectangle* short_side_ptr1 = new Rectangle(p0, a, b);
	short_side_ptr1->SetMaterial(matte_ptr7);
	AddObject(short_side_ptr1);
	
	
	// side 2
    
    Matte* matte_ptr8 = new Matte;
	matte_ptr8->SetKa(0.0);
	matte_ptr8->SetKd(0.6);
	matte_ptr8->SetColor(gWhite);	 // white
	matte_ptr8->SetSampler(new MultiJittered(numSamples));
	
	p0 = Vector3(8.2, 0.0, 22.5);
	a = Vector3(15.8, 0.0, 4.7);
	Rectangle* short_side_ptr2 = new Rectangle(p0, a, b);
	short_side_ptr2->SetMaterial(matte_ptr8);
	AddObject(short_side_ptr2);
	
	
	// side 3
    
    Matte* matte_ptr9 = new Matte;
	matte_ptr9->SetKa(0.0);
	matte_ptr9->SetKd(0.6);
	matte_ptr9->SetColor(gWhite);	 // white
	matte_ptr9->SetSampler(new MultiJittered(numSamples));

	p0 = Vector3(24.2, 0.0, 27.4);
	a = Vector3(4.8, 0.0, -16.0);
	Rectangle* short_side_ptr3 = new Rectangle(p0, a, b);
	short_side_ptr3->SetMaterial(matte_ptr9);
	AddObject(short_side_ptr3);
	
	
	// side 4
    
    Matte* matte_ptr10 = new Matte;
	matte_ptr10->SetKa(0.0);
	matte_ptr10->SetKd(0.6);
	matte_ptr10->SetColor(gWhite);	 // white
	matte_ptr10->SetSampler(new MultiJittered(numSamples));
	
	p0 = Vector3(29.0, 0.0, 11.4);
	a = Vector3(-16.0, 0.0, -4.9);
	Rectangle* short_side_ptr4 = new Rectangle(p0, a, b);
	short_side_ptr4->SetMaterial(matte_ptr10);
	AddObject(short_side_ptr4);
	
	
	
	
	// tall box
	
	// top
    
    Matte* matte_ptr11 = new Matte;
	matte_ptr11->SetKa(0.0);
	matte_ptr11->SetKd(0.6);
	matte_ptr11->SetColor(gWhite);	 // white
	matte_ptr11->SetSampler(new MultiJittered(numSamples));
	
	p0 = Vector3(42.3, 33.0, 24.7);
	a = Vector3(-15.8, 0.0, 4.9);
	b = Vector3(4.9, 0.0, 15.9);
	normal = Vector3(0.0, 1.0, 0.0);
	Rectangle* tall_top_ptr = new Rectangle(p0, a, b, normal);
	tall_top_ptr->SetMaterial(matte_ptr11);
	AddObject(tall_top_ptr);
	
	
	// side 1
    
    Matte* matte_ptr12 = new Matte;
	matte_ptr12->SetKa(0.0);
	matte_ptr12->SetKd(0.6);
	matte_ptr12->SetColor(gWhite);	 // white
	matte_ptr12->SetSampler(new MultiJittered(numSamples));
	
	p0 = Vector3(42.3, 0.0, 24.7);
	a = Vector3(-15.8, 0.0, 4.9);
	b = Vector3(0.0, 33.0, 0.0);
	Rectangle* tall_side_ptr1 = new Rectangle(p0, a, b);
	tall_side_ptr1->SetMaterial(matte_ptr12);
	AddObject(tall_side_ptr1);
	
	
	// side 2
	
    Matte* matte_ptr13 = new Matte;
	matte_ptr13->SetKa(0.0);
	matte_ptr13->SetKd(0.6);
	matte_ptr13->SetColor(gWhite);	 // white
	matte_ptr13->SetSampler(new MultiJittered(numSamples));
    
	p0 = Vector3(26.5, 0.0, 29.6);
	a = Vector3(4.9, 0.0, 15.9);
	Rectangle* tall_side_ptr2 = new Rectangle(p0, a, b);
	tall_side_ptr2->SetMaterial(matte_ptr13);
	AddObject(tall_side_ptr2);
	
	
	// side 3
    
    Matte* matte_ptr14 = new Matte;
	matte_ptr14->SetKa(0.0);
	matte_ptr14->SetKd(0.6);
	matte_ptr14->SetColor(gWhite);	 // white
	matte_ptr14->SetSampler(new MultiJittered(numSamples));
	
	p0 = Vector3(31.4, 0.0, 45.5);
	a = Vector3(15.8, 0.0, -4.9);
	Rectangle* tall_side_ptr3 = new Rectangle(p0, a, b);
	tall_side_ptr3->SetMaterial(matte_ptr14);
	AddObject(tall_side_ptr3);
	
	
	// side 4
    
    Matte* matte_ptr15 = new Matte;
	matte_ptr15->SetKa(0.0);
	matte_ptr15->SetKd(0.6);
	matte_ptr15->SetColor(gWhite);	 // white
	matte_ptr15->SetSampler(new MultiJittered(numSamples));
	
	p0 = Vector3(47.2, 0.0, 40.6);
	a = Vector3(-4.9, 0.0, -15.9);
	Rectangle* tall_side_ptr4 = new Rectangle(p0, a, b);
	tall_side_ptr4->SetMaterial(matte_ptr15);
	AddObject(tall_side_ptr4);
}