//
//  Matte.cpp
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/22/14.
//
//

#include "Matte.h"
#include "../World/World.h"

namespace RayTracer
{
Matte::Matte()
    : mAmbient(new Lambertian)
    , mDiffuse(new Lambertian)
{
}

Matte::Matte(const Matte& o)
{
    //TODO: Clone ptrs
}

Color Matte::Shade(ShadeRec& sr)
{
    Vector3 wo = -sr.ray.d;
    Color color = mAmbient->Rho(sr, wo) * sr.world.GetAmbientLight().L(sr);

    for(const LightPtr& pLight : sr.world.GetLights()) {
        Vector3 wi = pLight->GetDirection(sr);
        Real nDotWi = glm::dot(sr.normal, wi);
        if(nDotWi > 0.0f) {
            bool inShadow = false;
            if(pLight->CastShadows()) {
                Ray shadowRay(sr.hitPoint, wi);
                inShadow = pLight->InShadow(shadowRay, sr);
            }

            if(!inShadow) {
                color += mDiffuse->F(sr, wi, wo) * pLight->L(sr) * nDotWi;
            }
        }
    }

    return color;
}

Color Matte::AreaLightShade(ShadeRec& sr)
{
    Vector3 wo = -sr.ray.d;
    Color color = mAmbient->Rho(sr, wo) * sr.world.GetAmbientLight().L(sr);

    for(const LightPtr& pLight : sr.world.GetLights()) {
        Vector3 wi = pLight->GetDirection(sr);
        Real nDotWi = glm::dot(sr.normal, wi);
        if(nDotWi > 0.0f) {
            bool inShadow = false;
            if(pLight->CastShadows()) {
                Ray shadowRay(sr.hitPoint, wi);
                inShadow = pLight->InShadow(shadowRay, sr);
            }

            if(!inShadow) {
                color += mDiffuse->F(sr, wi, wo) * pLight->L(sr) * pLight->G(sr) * nDotWi / pLight->Pdf(sr);
            }
        }
    }

    return color;
}
    
Color Matte::PathShade(ShadeRec& sr)
{
    Vector3 wi;
    Vector3 wo = -sr.ray.d;
    Real pdf;
    
    Color f = mDiffuse->SampleF(sr, wi, wo, pdf);
    Real nDotWi = glm::dot(sr.normal, wi);
    Ray reflectedRay(sr.hitPoint, wi);
    
    return (f * sr.world.GetTracer().TraceRay(reflectedRay, sr.depth + 1) * nDotWi / pdf);
}
    
Matte* Matte::Clone() const
{
    return new Matte(*this);
}
} //end of namespace