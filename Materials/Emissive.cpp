//
//  Emissive.cpp
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/25/14.
//
//

#include "Emissive.h"

namespace RayTracer
{
Emissive::Emissive()
    : mLs(1.0f)
    , mColor(gWhite)
{
}

Color Emissive::Shade(ShadeRec& sr)
{
    if(glm::dot(-sr.normal, sr.ray.d) > 0.0f) {
        return mLs * mColor;
    } else {
        return gBlack;
    }
}
    
Color Emissive::PathShade(ShadeRec& sr)
{
    return Emissive::Shade(sr);
}

Color Emissive::AreaLightShade(ShadeRec& sr)
{
    return Emissive::Shade(sr);
}

Color Emissive::GetL(ShadeRec& sr) const
{
    return mLs * mColor;
}
} //end of namespace