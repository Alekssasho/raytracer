//
//  Material.h
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/22/14.
//
//

#ifndef RayTracer_Material_h
#define RayTracer_Material_h

#include "../Utilities/Math.h"
#include <memory>

namespace RayTracer
{
class Material
{
public:
    virtual ~Material() {}
    virtual Material* Clone() const = 0;
    virtual Color Shade(ShadeRec& sr) { return gBlack; }
    virtual Color AreaLightShade(ShadeRec& sr) { return gBlack; }
    virtual Color PathShade(ShadeRec& sr) { return gBlack; }
    virtual Color GetL(ShadeRec& sr) const { return gBlack; }
};

using MaterialPtr = std::unique_ptr<Material>;
} //end of namespace

#endif
