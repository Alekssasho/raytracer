//
//  Emissive.h
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/25/14.
//
//

#ifndef __RayTracer__Emissive__
#define __RayTracer__Emissive__

#include "Material.h"

namespace RayTracer
{
class Emissive : public Material
{
public:
    Emissive();

    Emissive* Clone() const override { return new Emissive(*this); }

    Color Shade(ShadeRec& sr) override;
    Color AreaLightShade(ShadeRec& sr) override;
    Color PathShade(ShadeRec& sr) override;

    Color GetL(ShadeRec& sr) const override;

    void SetColor(Color c) { mColor = c; }
    void ScaleRadiance(Real ls) { mLs = ls; }

private:
    Real mLs;
    Color mColor;
};
} //end of namespace

#endif /* defined(__RayTracer__Emissive__) */
