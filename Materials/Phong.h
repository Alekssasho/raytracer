//
//  Phong.h
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/22/14.
//
//

#ifndef __RayTracer__Phong__
#define __RayTracer__Phong__

#include "Material.h"
#include "../BRDF/Lambertian.h"
#include "../BRDF/GlossySpecular.h"
#include <memory>

namespace RayTracer
{
class Phong : public Material
{
public:
    Phong();
    
    Phong(const Phong& o);

    Color Shade(ShadeRec& sr) override;
    Color AreaLightShade(ShadeRec& sr) override;

    Phong* Clone() const override;

    void SetExponent(Real exp) { mpSpecular->SetExponent(exp); }
    void SetKa(Real ka) { mpAmbient->SetKd(ka); }
    void SetKd(Real kd) { mpDiffuse->SetKd(kd); }
    void SetKs(Real ks) { mpSpecular->SetKs(ks); }
    void SetColor(Color c)
    {
        mpAmbient->SetCd(c);
        mpDiffuse->SetCd(c);
        mpSpecular->SetColor(c);
    }

private:
    using LambertianPtr = std::unique_ptr<Lambertian>;
    using GlossyPtr = std::unique_ptr<GlossySpecular>;

    LambertianPtr mpAmbient;
    LambertianPtr mpDiffuse;
    GlossyPtr mpSpecular;
};
} //end of namespace
#endif /* defined(__RayTracer__Phong__) */
