//
//  GlossyReflector.h
//  RayTracer
//
//  Created by Aleksandar Angelov on 8/25/14.
//
//

#ifndef __RayTracer__GlossyReflector__
#define __RayTracer__GlossyReflector__

#include "Phong.h"
#include "../BRDF/GlossySpecular.h"

namespace RayTracer
{
    
    class GlossyReflector : public Phong
    {
    public:
        GlossyReflector();
        
        void SetSamples(int numSamples, Real exp);
        void SetKr(Real kr);
        void SetCr(Color c);
        void SetReflectiveExponent(Real exp);
        
        Color AreaLightShade(ShadeRec& sr) override;
    private:
        using GlossyPtr = std::unique_ptr<GlossySpecular>;
        GlossyPtr mpGlossySpecular;
    };
    
}//end of namespace

#endif /* defined(__RayTracer__GlossyReflector__) */
