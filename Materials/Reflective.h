//
//  Reflective.h
//  RayTracer
//
//  Created by Aleksandar Angelov on 8/22/14.
//
//

#ifndef __RayTracer__Reflective__
#define __RayTracer__Reflective__

#include "Phong.h"
#include "../BRDF/PerfectSpecular.h"

namespace RayTracer
{
    
    class Reflective : public Phong
    {
    public:
        Reflective();
        
        Color Shade(ShadeRec& sr) override;
        Color AreaLightShade(ShadeRec& sr) override;
        Color PathShade(ShadeRec& sr) override;
        
        void SetKr(Real kr) { mpReflective->ScaleRadiance(kr); }
        void SetCr(Color c) { mpReflective->SetColor(c); }
        
    private:
        using PerfectSpecularPtr = std::unique_ptr<PerfectSpecular>;
        PerfectSpecularPtr mpReflective;
    };
    
}//end of namespace

#endif /* defined(__RayTracer__Reflective__) */
