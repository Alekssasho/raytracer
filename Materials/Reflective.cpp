//
//  Reflective.cpp
//  RayTracer
//
//  Created by Aleksandar Angelov on 8/22/14.
//
//

#include "Reflective.h"
#include "../World/World.h"

namespace RayTracer
{
    Reflective::Reflective()
    :Phong(), mpReflective(new PerfectSpecular)
    {
    }
    
    Color Reflective::Shade(ShadeRec& sr)
    {
        Color color = Phong::Shade(sr);
        
        Vector3 wo = -sr.ray.d;
        Vector3 wi;
        Color fr = mpReflective->SampleF(sr, wi, wo);
        Ray reflectedRay(sr.hitPoint, wi);
        
        color += fr * sr.world.GetTracer().TraceRay(reflectedRay, sr.depth + 1) * glm::dot(sr.normal, wi);
        
        return color;
    }
    
    Color Reflective::AreaLightShade(ShadeRec& sr)
    {
        return this->Shade(sr);
    }
    
    Color Reflective::PathShade(ShadeRec& sr)
    {
        Vector3 wo = -sr.ray.d;
        Vector3 wi;
        Real pdf;
        
        Color fr = mpReflective->SampleF(sr, wi, wo, pdf);
        Ray reflectedRay(sr.hitPoint, wi);
        
        return (fr * sr.world.GetTracer().TraceRay(reflectedRay, sr.depth + 1) * glm::dot(sr.normal, wi) / pdf);
    }
}//end of namespace