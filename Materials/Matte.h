//
//  Matte.h
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/22/14.
//
//

#ifndef __RayTracer__Matte__
#define __RayTracer__Matte__

#include "Material.h"
#include "../BRDF/Lambertian.h"
#include <memory>

namespace RayTracer
{

class Matte : public Material
{
public:
    Matte();

    Matte(const Matte& o);

    Color Shade(ShadeRec& sr) override;
    Color AreaLightShade(ShadeRec& sr) override;
    Color PathShade(ShadeRec& sr) override;
    Matte* Clone() const override;
    //TODO: needs to copy lambertian ptr;

    void SetKa(Real k) { mAmbient->SetKd(k); }
    void SetKd(Real k) { mDiffuse->SetKd(k); }
    void SetColor(Color c) { mAmbient->SetCd(c), mDiffuse->SetCd(c); }
    
    void SetSampler(Sampler* pSampler) { mDiffuse->SetSampler(pSampler); }

private:
    using LambertianPtr = std::unique_ptr<Lambertian>;
    LambertianPtr mAmbient;
    LambertianPtr mDiffuse;
};
} //end of namespace

#endif /* defined(__RayTracer__Matte__) */
