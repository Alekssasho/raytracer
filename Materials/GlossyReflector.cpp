//
//  GlossyReflector.cpp
//  RayTracer
//
//  Created by Aleksandar Angelov on 8/25/14.
//
//

#include "GlossyReflector.h"
#include "../World/World.h"


namespace RayTracer
{
    
    GlossyReflector::GlossyReflector()
    : Phong(),
    mpGlossySpecular(new GlossySpecular)
    {
    }
    
    void GlossyReflector::SetSamples(int numSamples, Real exp)
    {
        mpGlossySpecular->SetSamples(numSamples, exp);
    }
    
    void GlossyReflector::SetKr(Real kr)
    {
        mpGlossySpecular->SetKs(kr);
    }
    
    void GlossyReflector::SetCr(Color c)
    {
        mpGlossySpecular->SetColor(c);
    }
    
    void GlossyReflector::SetReflectiveExponent(Real exp)
    {
        mpGlossySpecular->SetExponent(exp);
    }
    
    Color GlossyReflector::AreaLightShade(ShadeRec& sr)
    {
        Color color(Phong::AreaLightShade(sr));
        
        Vector3 wo(-sr.ray.d);
        Vector3 wi;
        Real pdf;
        Color fr(mpGlossySpecular->SampleF(sr, wi, wo, pdf));
        Ray reflectedRay(sr.hitPoint, wi);
        
        color += fr * sr.world.GetTracer().TraceRay(reflectedRay, sr.depth + 1) * (glm::dot(sr.normal, wi)) / pdf;
        
        return color;
    }
    
}//end of namespace