//
//  Phong.cpp
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/22/14.
//
//

#include "Phong.h"
#include "../World/World.h"

namespace RayTracer
{

Phong::Phong()
    : mpAmbient(new Lambertian)
    , mpDiffuse(new Lambertian)
    , mpSpecular(new GlossySpecular)
{
}

Color Phong::Shade(ShadeRec& sr)
{
    Vector3 wo = -sr.ray.d;
    Color color = mpAmbient->Rho(sr, wo) * sr.world.GetAmbientLight().L(sr);

    for(const auto& pLight : sr.world.GetLights()) {
        Vector3 wi = pLight->GetDirection(sr);
        Real nDotwi = glm::dot(sr.normal, wi);

        if(nDotwi > 0.0f) {
            
            bool inShadow = false;
            if (pLight->CastShadows()) {
                Ray shadowRay(sr.hitPoint, wi);
                inShadow = pLight->InShadow(shadowRay, sr);
            }
            
            if (!inShadow) {
                color += (mpDiffuse->F(sr, wi, wo) + mpSpecular->F(sr, wi, wo)) * pLight->L(sr) * nDotwi;
            }
        }
    }

    return color;
}

Color Phong::AreaLightShade(ShadeRec& sr)
{
    Vector3 wo = -sr.ray.d;
    Color color = mpAmbient->Rho(sr, wo) * sr.world.GetAmbientLight().L(sr);

    for(const auto& pLight : sr.world.GetLights()) {
        Vector3 wi = pLight->GetDirection(sr);
        Real nDotwi = glm::dot(sr.normal, wi);

        if(nDotwi > 0.0f) {

            bool inShadow = false;
            if(pLight->CastShadows()) {
                Ray shadowRay(sr.hitPoint, wi);
                inShadow = pLight->InShadow(shadowRay, sr);
            }

            if(!inShadow) {
                color += (mpDiffuse->F(sr, wi, wo) + mpSpecular->F(sr, wi, wo)) * pLight->L(sr) * pLight->G(sr) * nDotwi / pLight->Pdf(sr);
            }
        }
    }

    return color;
}

Phong* Phong::Clone() const
{
    return new Phong(*this);
}

Phong::Phong(const Phong& o)
{
    //TODO: implement me
}

} //end of namespace