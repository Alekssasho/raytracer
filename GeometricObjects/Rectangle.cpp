#include "Rectangle.h"
#include <glm/gtx/norm.hpp>

namespace RayTracer
{

const Real Rectangle::sEpsilon = 0.001f;

Rectangle::Rectangle(const Vector3& p0, const Vector3& a, const Vector3& b)
:GeometricObject()
, mP0(p0)
, mA(a)
, mB(b)
, mNormal(glm::normalize(glm::cross(mA, mB)))
, mInvArea(1.0f / (glm::length(mA) * glm::length(mB)))
{}

Rectangle::Rectangle(const Vector3& p0, const Vector3& a, const Vector3& b, const Vector3& normal)
:GeometricObject()
, mP0(p0)
, mA(a)
, mB(b)
, mNormal(glm::normalize(normal))
, mInvArea(1.0f / (glm::length(mA) * glm::length(mB)))
{}

    
Vector3 Rectangle::Sample()
{
    auto sp = mpSampler->SampleUnitSquare();
    return mP0 + sp.x * mA + sp.y * mB;
}

bool Rectangle::Hit(const Ray& ray, Real& tmin, ShadeRec& sr) const
{
    Real t = glm::dot(mP0 - ray.o, mNormal) / glm::dot(ray.d, mNormal);

    if(t < sEpsilon) {
        return false;
    }

    Vector3 p = ray.o + t * ray.d;
    Vector3 d = p - mP0;

    Real dDotA = glm::dot(d, mA);
    if(dDotA < 0.0f || dDotA > glm::length2(mA))
        return false;

    Real dDotB = glm::dot(d, mB);
    if(dDotB < 0.0f || dDotB > glm::length2(mB))
        return false;

    tmin = t;
    sr.normal = mNormal;
    sr.localHitPoint = p;

    return true;
}

bool Rectangle::ShadowHitConcrete(const Ray& ray, Real& tmin) const
{
    Real t = glm::dot(mP0 - ray.o, mNormal) / glm::dot(ray.d, mNormal);

    if(t < sEpsilon) {
        return false;
    }

    Vector3 p = ray.o + t * ray.d;
    Vector3 d = p - mP0;

    Real dDotA = glm::dot(d, mA);
    if(dDotA < 0.0f || dDotA > glm::length2(mA))
        return false;

    Real dDotB = glm::dot(d, mB);
    if(dDotB < 0.0f || dDotB > glm::length2(mB))
        return false;

    tmin = t;
    return true;
}

}//end of namespace
