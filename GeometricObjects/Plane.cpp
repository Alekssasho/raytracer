//
//  Plane.cpp
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/9/14.
//
//

#include "Plane.h"

namespace RayTracer
{

const Real Plane::sEpsilon = 0.01f;
    
Plane::Plane()
    : GeometricObject()
    , mPoint(0.0f)
    , mNormal(gUp)
{
}

Plane::Plane(const Vector3& point, const Vector3& normal)
    : GeometricObject()
    , mPoint(point)
    , mNormal(glm::normalize(normal))
{
}

Plane* Plane::Clone() const
{
    return new Plane(*this);
}

bool Plane::Hit(const Ray& ray, Real& tmin, ShadeRec& sr) const
{
    Real t = glm::dot((mPoint - ray.o), mNormal) / glm::dot(ray.d, mNormal);
    if(t > sEpsilon) {
        tmin = t;
        sr.normal = mNormal;
        sr.localHitPoint = ray.o + t * ray.d;

        return true;
    }

    return false;
}

bool Plane::ShadowHitConcrete(const Ray& ray, Real& tmin) const
{
    Real t = glm::dot((mPoint - ray.o), mNormal) / glm::dot(ray.d, mNormal);
    if(t > sEpsilon) {
        tmin = t;
        return true;
    }
    return false;
}
} //end of namespace