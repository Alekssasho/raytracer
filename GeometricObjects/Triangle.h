//
//  Triangle.h
//  RayTracer
//
//  Created by Aleksandar Angelov on 8/5/14.
//
//

#ifndef __RayTracer__Triangle__
#define __RayTracer__Triangle__

#include "GeometricObject.h"

namespace RayTracer
{
    class Triangle : public GeometricObject
    {
    public:
        Triangle(const Vector3& p0, const Vector3& p1, const Vector3& p2);
        
        GeometricObject* Clone() const override { return new Triangle(*this); };
        
        bool Hit(const Ray& ray, Real& t, ShadeRec& shadeRec) const override;
        
    protected:
        bool ShadowHitConcrete(const Ray& ray, Real& t) const override;
        
    private:
        Vector3 v0, v1, v2;
        Vector3 normal;
    };
}//end of namespace

#endif /* defined(__RayTracer__Triangle__) */
