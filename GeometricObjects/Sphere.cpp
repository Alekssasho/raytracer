//
//  Sphere.cpp
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/10/14.
//
//

#include "Sphere.h"

namespace RayTracer
{

const Real Sphere::sEpsilon = 0.01f;

Sphere::Sphere()
    : mCenter(0.0f)
    , mRadius(1.0f)
{
}

Sphere::Sphere(Vector3 center, Real radius)
    : mCenter(center)
    , mRadius(radius)
{
}

Sphere* Sphere::Clone() const
{
    return new Sphere(*this);
}

bool Sphere::Hit(const Ray& ray, Real& tmin, ShadeRec& sr) const
{
    //we need to solve quadratic equation: at^2 + bt + c = 0
    Vector3 temp = ray.o - mCenter;
    Real a = glm::dot(ray.d, ray.d);
    Real b = 2.0f * glm::dot(temp, ray.d);
    Real c = glm::dot(temp, temp) - mRadius * mRadius;
    Real disc = b * b - 4.0f * a * c;

    if(disc < 0.0f) {
        return false;
    } else {
        Real e = glm::sqrt(disc);
        Real denom = 2.0f * a;
        Real t = (-b - e) / denom; //smaller root

        if(t > sEpsilon) {
            tmin = t;
            sr.normal = (temp + t * ray.d) / mRadius;
            sr.localHitPoint = ray.o + t * ray.d;
            return true;
        }

        t = (-b + e) / denom; //bigger root

        if(t > sEpsilon) {
            tmin = t;
            sr.normal = (temp + t * ray.d) / mRadius;
            sr.localHitPoint = ray.o + t * ray.d;
            return true;
        }
    }
    return false;
}

bool Sphere::ShadowHitConcrete(const Ray& ray, Real& tmin) const
{
    //we need to solve quadratic equation: at^2 + bt + c = 0
    Vector3 temp = ray.o - mCenter;
    Real a = glm::dot(ray.d, ray.d);
    Real b = 2.0f * glm::dot(temp, ray.d);
    Real c = glm::dot(temp, temp) - mRadius * mRadius;
    Real disc = b * b - 4.0f * a * c;

    if(disc < 0.0f) {
        return false;
    } else {
        Real e = glm::sqrt(disc);
        Real denom = 2.0f * a;
        Real t = (-b - e) / denom; //smaller root

        if(t > sEpsilon) {
            tmin = t;
            return true;
        }

        t = (-b + e) / denom; //bigger root

        if(t > sEpsilon) {
            tmin = t;
            return true;
        }
    }
    return false;
}
} //end of namespace