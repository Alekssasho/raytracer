//
//  Triangle.cpp
//  RayTracer
//
//  Created by Aleksandar Angelov on 8/5/14.
//
//

#include "Triangle.h"


namespace RayTracer
{
    Triangle::Triangle(const Vector3& p0, const Vector3& p1, const Vector3& p2)
    :v0(p0), v1(p1), v2(p2)
    {
        using namespace glm;
        normal = normalize(cross(v1 - v0, v2 - v0));
    }
    
    bool Triangle::Hit(const Ray &ray, Real &tmin, ShadeRec &sr) const
    {
        Real a = v0.x - v1.x, b = v0.x - v2.x, c = ray.d.x, d = v0.x - ray.o.x;
        Real e = v0.y - v1.y, f = v0.y - v2.y, g = ray.d.y, h = v0.y - ray.o.y;
        Real i = v0.z - v1.z, j = v0.z - v2.z, k = ray.d.z, l = v0.z - ray.o.z;
        
        Real m = f * k - g * j, n = h * k - g * l, p = f * l - h * j;
        Real q = g * i - e * k, s = e * j - f * i;
        
        Real invDenom = 1.0f / (a * m + b * q + c * s);
        
        Real e1 = d * m - b * n - c * p;
        Real beta = e1 * invDenom;
        
        if (beta < 0.0f) {
            return false;
        }
        
        Real r = e * l - h * i;
        Real e2 = a * n + d * q + c * r;
        Real gamma = e2 * invDenom;
        if (gamma < 0.0f) {
            return false;
        }
        
        if(beta + gamma > 1.0f)
            return false;
        
        Real e3 = a * p - b * r + d * s;
        Real t = e3 * invDenom;
        
        if(t < kEpsilon)
            return false;
        
        tmin = t;
        sr.normal = normal;
        sr.localHitPoint = ray.o + t * ray.d;
        
        return true;
    }
    
    bool Triangle::ShadowHitConcrete(const Ray& ray, Real& tmin) const
    {
        Real a = v0.x - v1.x, b = v0.x - v2.x, c = ray.d.x, d = v0.x - ray.o.x;
        Real e = v0.y - v1.y, f = v0.y - v2.y, g = ray.d.y, h = v0.y - ray.o.y;
        Real i = v0.z - v1.z, j = v0.z - v2.z, k = ray.d.z, l = v0.z - ray.o.z;
        
        Real m = f * k - g * j, n = h * k - g * l, p = f * l - h * j;
        Real q = g * i - e * k, s = e * j - f * i;
        
        Real invDenom = 1.0f / (a * m + b * q + c * s);
        
        Real e1 = d * m - b * n - c * p;
        Real beta = e1 * invDenom;
        
        if (beta < 0.0f) {
            return false;
        }
        
        Real r = e * l - h * i;
        Real e2 = a * n + d * q + c * r;
        Real gamma = e2 * invDenom;
        if (gamma < 0.0f) {
            return false;
        }
        
        if(beta + gamma > 1.0f)
            return false;
        
        Real e3 = a * p - b * r + d * s;
        Real t = e3 * invDenom;
        
        if(t < kEpsilon)
            return false;
        
        tmin = t;

        
        return true;
    }
}//end of namespace