//
//  Sphere.h
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/10/14.
//
//

#ifndef __RayTracer__Sphere__
#define __RayTracer__Sphere__

#include "GeometricObject.h"

namespace RayTracer
{
class Sphere : public GeometricObject
{
public:
    Sphere();
    Sphere(Vector3 center, Real radius);

    virtual Sphere* Clone() const override;

    bool Hit(const Ray& ray, Real& t, ShadeRec& shadeRec) const override;
    bool ShadowHitConcrete(const Ray& ray, Real& t) const override;

    void SetCenter(const Vector3& center) { mCenter = center; }
    void SetRadius(Real rad) { mRadius = rad; }

private:
    Vector3 mCenter;
    Real mRadius;
    
    static const Real sEpsilon;
};
} //end of namespace

#endif /* defined(__RayTracer__Sphere__) */
