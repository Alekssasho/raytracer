//
//  GeometricObject.h
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/9/14.
//
//

#ifndef __RayTracer__GeometricObject__
#define __RayTracer__GeometricObject__

#include "../Utilities/Math.h"
#include "../Materials/Material.h"
#include "../Utilities/BBox.h"
#include <memory>

namespace RayTracer
{
class GeometricObject
{
public:
    GeometricObject() = default;

    //TODO: need to clone material
    GeometricObject(const GeometricObject& o);
    GeometricObject& operator=(const GeometricObject& o);

    virtual GeometricObject* Clone() const = 0;
    virtual ~GeometricObject() {}

    virtual bool Hit(const Ray& ray, Real& t, ShadeRec& shadeRec) const = 0;
    bool ShadowHit(const Ray& ray, Real& t) const;

    virtual Vector3 Sample() { return Vector3(); }
    virtual Real Pdf(const ShadeRec& sr) { return 0.0f; }
    virtual Vector3 GetNormal(const Vector3& p) { return Vector3(); }

    Material* GetMaterial() const { return mpMat.get(); }
    void SetMaterial(Material* pMat) { mpMat.reset(pMat); }

    void SetShadows(bool s) { mShadow = s; }
    
    virtual BBox GetBoundingBox() { return BBox(); }

protected:
    MaterialPtr mpMat;
    bool mShadow = true;

    virtual bool ShadowHitConcrete(const Ray& ray, Real& t) const = 0;
};

using GeomObjectPtr = std::unique_ptr<GeometricObject>;

} //end of namespace

#endif /* defined(__RayTracer__GeometricObject__) */
