//
//  Compound.h
//  RayTracer
//
//  Created by Aleksandar Angelov on 8/6/14.
//
//

#ifndef __RayTracer__Compound__
#define __RayTracer__Compound__

#include "GeometricObject.h"
#include <vector>

namespace RayTracer
{
    
    class Compound : public GeometricObject
    {
    public:
        
        bool Hit(const Ray& ray, Real& t, ShadeRec& shadeRec) const override;
        bool ShadowHitConcrete(const Ray& ray, Real& t) const override;
        
        Compound* Clone() const override { return new Compound(); }
        
    protected:
        std::vector<GeomObjectPtr> mObjects;
    };
    
}//end of namespace

#endif /* defined(__RayTracer__Compound__) */
