//
//  GeometricObject.cpp
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/9/14.
//
//

#include "GeometricObject.h"

namespace RayTracer
{
GeometricObject::GeometricObject(const GeometricObject& o)
    : mpMat(o.mpMat->Clone())
{
}

GeometricObject& GeometricObject::operator=(const GeometricObject& o)
{
    if(this != &o)
        mpMat.reset(o.mpMat->Clone());

    return *this;
}

bool GeometricObject::ShadowHit(const Ray& ray, Real& t) const
{
    if(mShadow)
        return this->ShadowHitConcrete(ray, t);
    else
        return false;
}

}