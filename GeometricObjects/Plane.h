//
//  Plane.h
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/9/14.
//
//

#ifndef __RayTracer__Plane__
#define __RayTracer__Plane__

#include "GeometricObject.h"

namespace RayTracer
{
class Plane : public GeometricObject
{
public:
    Plane();
    Plane(const Vector3& point, const Vector3& normal);

    virtual Plane* Clone() const override;

    bool Hit(const Ray& ray, Real& tmin, ShadeRec& sr) const override;
    bool ShadowHitConcrete(const Ray& ray, Real& t) const override;

private:
    Vector3 mPoint;
    Vector3 mNormal;
    
    static const Real sEpsilon;
};
} //end of namespace

#endif /* defined(__RayTracer__Plane__) */
