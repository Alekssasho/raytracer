#ifndef RAYTRACER_RECTANGLE_H
#define RAYTRACER_RECTANGLE_H

#include "GeometricObject.h"
#include "../Samplers/Sampler.h"

namespace RayTracer
{
class Rectangle : public GeometricObject
{
public:
    Rectangle(const Vector3& po, const Vector3& a, const Vector3& b);
    Rectangle(const Vector3& po, const Vector3& a, const Vector3& b, const Vector3& normal);
    Rectangle(const Rectangle& o) { }

    void SetSampler(Sampler* ps) { mpSampler.reset(ps); }

    Vector3 Sample() override;
    Real Pdf(const ShadeRec& sr) override { return mInvArea; }
    Vector3 GetNormal(const Vector3& p) override { return mNormal; }

    bool Hit(const Ray& ray, Real& tmin, ShadeRec& sr) const override;
    bool ShadowHitConcrete(const Ray& ray, Real& tmin) const override;

    Rectangle* Clone() const override { return new Rectangle(*this); }

private:
    Vector3 mP0;
    Vector3 mA;
    Vector3 mB;
    Vector3 mNormal;
    SamplerPtr mpSampler;
    Real mInvArea;

    static const Real sEpsilon;
};
}//end of namespace

#endif