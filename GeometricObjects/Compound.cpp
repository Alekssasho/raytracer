//
//  Compound.cpp
//  RayTracer
//
//  Created by Aleksandar Angelov on 8/6/14.
//
//

#include "Compound.h"

namespace RayTracer
{
    
    bool Compound::Hit(const Ray& ray, Real& tmin, ShadeRec& sr) const
    {
        Real t;
        bool hit = false;
        Vector3 normal;
        Vector3 localHitPoint;
        tmin = std::numeric_limits<Real>::max();
        
        for (const auto& pObj : mObjects){
            if (pObj->Hit(ray, t, sr) && t < tmin) {
                hit = true;
                tmin = t;
                normal = sr.normal;
                localHitPoint = sr.localHitPoint;
            }
        }
        
        if (hit) {
            sr.t = tmin;
            sr.normal = normal;
            sr.localHitPoint = localHitPoint;
        }
        
        return hit;
    }
    
    bool Compound::ShadowHitConcrete(const Ray& ray, Real& t) const
    {
        for(const auto& pObj : mObjects) {
            if (pObj->ShadowHit(ray, t)) {
                return true;
            }
        }
        
        return false;
    }
}//end of namespace