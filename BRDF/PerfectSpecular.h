//
//  PerfectSpecular.h
//  RayTracer
//
//  Created by Aleksandar Angelov on 8/22/14.
//
//

#ifndef __RayTracer__PerfectSpecular__
#define __RayTracer__PerfectSpecular__

#include "BRDF.h"

namespace RayTracer
{
    
    class PerfectSpecular : public BRDF
    {
    public:
        Color SampleF(const ShadeRec& sr, Vector3& wi, const Vector3& wo) const override;
        Color SampleF(const ShadeRec& sr, Vector3& wi, const Vector3& wo, Real& pdf) const override;
        Color F(const ShadeRec& sr, const Vector3& wi, const Vector3& wo) const override { return gBlack; }
        Color Rho(const ShadeRec& sr, const Vector3& wo) const override { return gBlack; }
        
        void ScaleRadiance(Real kr) { mKr = kr; }
        void SetColor(Color color) { mCr = color; }
        
    private:
        Real mKr;// reflection koefficient
        Color mCr;//reflection color
    };
    
}//end of namespace

#endif /* defined(__RayTracer__PerfectSpecular__) */
