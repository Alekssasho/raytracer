//
//  Lambertian.cpp
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/18/14.
//
//

#include "Lambertian.h"

namespace RayTracer
{
Lambertian::Lambertian()
    : mKd(0.0f)
    , mCd(gBlack)
{
}

Color Lambertian::F(const ShadeRec& sr, const Vector3& wi, const Vector3& wo) const
{
    return mKd * mCd * INV_PI;
}

Color Lambertian::SampleF(const ShadeRec& sr, Vector3& wi, const Vector3& wo) const
{
    //To be implemented later
    return gBlack;
}

Color Lambertian::Rho(const ShadeRec& sr, const Vector3& wo) const
{
    return mKd * mCd;
}
    
Color Lambertian::SampleF(const ShadeRec& sr, Vector3& wi, const Vector3& wo, Real& pdf) const
{
    using namespace glm;
    Vector3 w = sr.normal;
    Vector3 v = normalize(cross(Vector3(0.0034f, 1.0f, 0.0071f), w));
    Vector3 u = cross(v, w);
    
    auto sp = mpSampler->SampleHemisphere();
    wi = normalize(sp.x * u + sp.y * v + sp.z * w);
    pdf = dot(sr.normal, wi) * INV_PI;
    
    return mKd * mCd * INV_PI;
}

} //end of namespace