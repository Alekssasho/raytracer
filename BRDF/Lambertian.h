//
//  Lambertian.h
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/18/14.
//
//

#ifndef __RayTracer__Lambertian__
#define __RayTracer__Lambertian__

#include "BRDF.h"

namespace RayTracer
{
class Lambertian : public BRDF
{
public:
    Lambertian();

    virtual Color F(const ShadeRec& sr, const Vector3& wi, const Vector3& wo) const override;
    virtual Color SampleF(const ShadeRec& sr, Vector3& wi, const Vector3& wo) const override;
    Color SampleF(const ShadeRec& sr, Vector3& wi, const Vector3& wo, Real& pdf) const override;
    virtual Color Rho(const ShadeRec& sr, const Vector3& wo) const override;

    void SetKd(Real kd) { mKd = kd; }
    void SetCd(const Color& c) { mCd = c; }

private:
    Real mKd;
    Color mCd;
};
} //end of namespace

#endif /* defined(__RayTracer__Lambertian__) */
