//
//  GlossySpecular.h
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/22/14.
//
//

#ifndef __RayTracer__GlossySpecular__
#define __RayTracer__GlossySpecular__

#include "BRDF.h"
#include "../Samplers/Sampler.h"

namespace RayTracer
{
class GlossySpecular : public BRDF
{
public:
    GlossySpecular();

    Color F(const ShadeRec& sr, const Vector3& wi, const Vector3& wo) const override;
    Color SampleF(const ShadeRec& sr, Vector3& wi, const Vector3& wo) const override;
    Color SampleF(const ShadeRec& sr, Vector3& wi, const Vector3& wo, Real& pdf) const override;
    Color Rho(const ShadeRec& sr, const Vector3& wo) const override;

    void SetKs(Real ks) { mKs = ks; }
    void SetColor(Color c) { mCs = c; }
    void SetExponent(Real exp) { mExp = exp; }
    
    void SetSamples(int numSamples, Real exp);
    void SetSampler(Sampler* pSampler, Real exp);
    
private:
    Real mKs;
    Color mCs;
    Real mExp;
    
    SamplerPtr mpSampler;
};
} //end of namespace

#endif /* defined(__RayTracer__GlossySpecular__) */
