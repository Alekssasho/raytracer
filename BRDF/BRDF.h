//
//  BRDF.h
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/18/14.
//
//

#ifndef RayTracer_BRDF_h
#define RayTracer_BRDF_h

#include "../Utilities/Math.h"
#include "../Samplers/Sampler.h"

namespace RayTracer
{
class BRDF
{
public:
    virtual ~BRDF() {}

    //Take ownership of the object
    void SetSampler(Sampler* pSampler)
    {
        mpSampler.reset(pSampler);
        mpSampler->MapSamplesToHemisphere(1);
    }

    virtual Color F(const ShadeRec& sr, const Vector3& wi, const Vector3& wo) const = 0;
    virtual Color SampleF(const ShadeRec& sr, Vector3& wi, const Vector3& wo) const = 0;
    virtual Color SampleF(const ShadeRec& sr, Vector3& wi, const Vector3& wo, Real& pdf) const { return gBlack; };
    virtual Color Rho(const ShadeRec& sr, const Vector3& wo) const = 0;

protected:
    SamplerPtr mpSampler;
};
    
} //end of namespace

#endif
