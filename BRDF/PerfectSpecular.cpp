//
//  PerfectSpecular.cpp
//  RayTracer
//
//  Created by Aleksandar Angelov on 8/22/14.
//
//

#include "PerfectSpecular.h"

namespace RayTracer
{
    Color PerfectSpecular::SampleF(const ShadeRec& sr, Vector3& wi, const Vector3& wo) const
    {
        Real nDotWo = glm::dot(sr.normal, wo);
        wi = -wo + 2.0f * sr.normal * nDotWo;
        
        return mKr * mCr / glm::dot(sr.normal, wi);
    }
    
    Color PerfectSpecular::SampleF(const ShadeRec& sr, Vector3& wi, const Vector3& wo, Real& pdf) const
    {
        Real nDotWo = glm::dot(wo, sr.normal);
        wi = -wo + 2.0f * sr.normal * nDotWo;
        pdf = glm::dot(sr.normal, wi);
        
        return mKr * mCr;
    }
}//end of namespace