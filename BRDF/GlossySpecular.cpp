//
//  GlossySpecular.cpp
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/22/14.
//
//

#include "GlossySpecular.h"
#include "../Samplers/MultiJittered.h"

namespace RayTracer
{
GlossySpecular::GlossySpecular()
    : mKs(0.0f)
    , mCs(gWhite)
    , mExp(1.0f)
{
}

Color GlossySpecular::F(const ShadeRec& sr, const Vector3& wi, const Vector3& wo) const
{
    Color color = gBlack;
    Real nDotWi = glm::dot(wi, sr.normal);
    Real rDotWo = glm::dot(wo, Vector3(-wi + 2.0f * sr.normal * nDotWi));
    if(rDotWo > 0.0f) {
        color = mCs * mKs * glm::pow(rDotWo, mExp);
    }

    return color;
}

Color GlossySpecular::SampleF(const ShadeRec& sr, Vector3& wi, const Vector3& wo) const
{
    return gBlack;
}
    
Color GlossySpecular::SampleF(const ShadeRec& sr, Vector3& wi, const Vector3& wo, Real& pdf) const
{
    using namespace glm;
    Real nDotWo = dot(sr.normal, wo);
    Vector3 r = -wo + 2.0f * sr.normal * nDotWo;
    
    Vector3 w = r;
    Vector3 u = normalize(cross(Vector3(0.00424, 1, 0.00764), w));
    Vector3 v = cross(u, w);
    
    Vector3 sp = mpSampler->SampleHemisphere();
    wi = sp.x * u + sp.y * v + sp.z * w;
    
    if (dot(sr.normal, wi) < 0.0f) {
        wi = -sp.x * u - sp.y * v + sp.z * w;
    }
    
    Real phongLobe = pow(dot(r, wi), mExp);
    pdf = phongLobe * dot(sr.normal, wi);
    
    return mKs * mCs * phongLobe;
}

Color GlossySpecular::Rho(const ShadeRec& sr, const Vector3& wo) const
{
    return gBlack;
}
    
    void GlossySpecular::SetSamples(int numSamples, Real exp)
    {
        this->SetSampler(new MultiJittered(numSamples), exp);
    }
    
    void GlossySpecular::SetSampler(Sampler* pSampler, Real exp)
    {
        mpSampler.reset(pSampler);
        mpSampler->MapSamplesToHemisphere(exp);
    }
    
} //end of namespace

