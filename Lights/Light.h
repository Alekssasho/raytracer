//
//  Light.h
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/22/14.
//
//

#ifndef RayTracer_Light_h
#define RayTracer_Light_h

#include "../Utilities/Math.h"

namespace RayTracer
{

class Light
{
public:
    virtual Vector3 GetDirection(ShadeRec& sr) = 0;

    virtual Color L(ShadeRec& sr)
    {
        return gBlack;
    }

    virtual ~Light() {}

    virtual bool CastShadows() const { return true; }

    virtual bool InShadow(Ray ray, const ShadeRec& sr)
    {
        return false;
    }

    virtual Real G(const ShadeRec& sr) const { return 1.0f; }
    virtual Real Pdf(const ShadeRec& sr) const { return 1.0f; }
};

} //end of namespace

#endif
