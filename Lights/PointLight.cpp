//
//  PointLight.cpp
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/22/14.
//
//

#include "PointLight.h"
#include "../World/World.h"

namespace RayTracer
{

PointLight::PointLight()
    : mLs(1.0f)
    , mColor(gWhite)
    , mPosition(0.0f)
{
}

Vector3 PointLight::GetDirection(ShadeRec& sr)
{
    return glm::normalize(mPosition - sr.hitPoint);
}

Color PointLight::L(ShadeRec& sr)
{
    return mLs * mColor;
}

bool PointLight::InShadow(Ray ray, const ShadeRec& sr)
{
    Real t;
    Real distance = glm::dot(mPosition - ray.o, ray.d);

    for(const auto& pObj : sr.world.GetObjects()) {
        if(pObj->ShadowHit(ray, t) && t < distance) {
            return true;
        }
    }

    return false;
}

} //end of namespace