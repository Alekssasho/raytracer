#ifndef RAYTRACER_AREALIGHT_H
#define RAYTRACER_AREALIGHT_H

#include "Light.h"
#include "../GeometricObjects/GeometricObject.h"

namespace RayTracer
{
    class AreaLight : public Light
    {
    public:
        AreaLight();

        void SetObject(GeometricObject* pOjb) { mpObject = pOjb; }

        Vector3 GetDirection(ShadeRec& sr) override;
        bool InShadow(Ray ray, const ShadeRec& sr) override;
        Color L(ShadeRec& sr) override;
        Real G(const ShadeRec& sr) const override;
        Real Pdf(const ShadeRec& sr) const override;
             
    private:
        GeometricObject* mpObject;
        Vector3 mpSamplePoint;
        Vector3 mLightNormal;
        Vector3 mWi;
    };
}//end of namespace

#endif