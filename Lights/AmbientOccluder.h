//
//  AmbientOccluder.h
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/23/14.
//
//

#ifndef __RayTracer__AmbientOccluder__
#define __RayTracer__AmbientOccluder__

#include "Light.h"
#include "../Samplers/Sampler.h"

namespace RayTracer
{

class AmbientOccluder : public Light
{
public:
    AmbientOccluder();

    void SetSampler(Sampler* pSampler);
    void SetMinAmount(Real c) { mMinAmount = c; }

    Vector3 GetDirection(ShadeRec& sr) override;
    Color L(ShadeRec& sr) override;
    bool InShadow(Ray ray, const ShadeRec& sr) override;

    void SetColor(Color c) { mColor = c; }
    void ScaleRadiance(Real ls) { mLs = ls; }

private:
    SamplerPtr mpSampler;
    Vector3 mU, mV, mW;
    Real mMinAmount;
    Color mColor;
    Real mLs;
};
} //end of namespace

#endif /* defined(__RayTracer__AmbientOccluder__) */
