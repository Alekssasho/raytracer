//
//  PointLight.h
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/22/14.
//
//

#ifndef __RayTracer__PointLight__
#define __RayTracer__PointLight__

#include "Light.h"

namespace RayTracer
{
class PointLight : public Light
{
public:
    PointLight();

    Vector3 GetDirection(ShadeRec& sr) override;
    Color L(ShadeRec& sr) override;
    bool InShadow(Ray ray, const ShadeRec& sr) override;

    void ScaleRadiance(Real ls) { mLs = ls; }
    void SetColor(Color c) { mColor = c; }
    void SetPosition(Vector3 pos) { mPosition = pos; }

private:
    Real mLs;
    Color mColor;
    Vector3 mPosition;
};
} //end of namespace

#endif /* defined(__RayTracer__PointLight__) */
