#include "AreaLight.h"
#include "../World/World.h"
#include <glm/gtx/norm.hpp>

namespace RayTracer
{
    AreaLight::AreaLight()
        :Light()
    {
    }

    Vector3 AreaLight::GetDirection(ShadeRec& sr)
    {
        mpSamplePoint = mpObject->Sample();
        mLightNormal = mpObject->GetNormal(mpSamplePoint);
        mWi = glm::normalize(mpSamplePoint - sr.hitPoint);

        return mWi;
    }

    bool AreaLight::InShadow(Ray ray, const ShadeRec& sr)
    {
        Real t;
        Real ts = glm::dot((mpSamplePoint - ray.o), ray.d);

        for(const auto& pObj : sr.world.GetObjects()) {
            if(pObj->ShadowHit(ray, t) && t < ts)
                return true;
        }
        return false;
    }

    Color AreaLight::L(ShadeRec& sr)
    {
        Real nDotD = glm::dot(-mLightNormal, mWi);
        if(nDotD > 0.0f)
            return mpObject->GetMaterial()->GetL(sr);
        else
            return gBlack;
    }

    Real AreaLight::G(const ShadeRec& sr) const
    {
        Real nDotD = glm::dot(-mLightNormal, mWi);
        Real d2 = glm::length2(mpSamplePoint - sr.hitPoint);

        return nDotD / d2;
    }

    Real AreaLight::Pdf(const ShadeRec& sr) const
    {
        return mpObject->Pdf(sr);
    }
}//end of namespace