//
//  AmbientOccluder.cpp
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/23/14.
//
//

#include "AmbientOccluder.h"
#include "../World/World.h"

namespace RayTracer
{

AmbientOccluder::AmbientOccluder()
    : mMinAmount(0.0f)
    , mColor(gWhite)
    , mLs(1.0f)
{
}

void AmbientOccluder::SetSampler(Sampler* pSampler)
{
    mpSampler.reset(pSampler);
    mpSampler->MapSamplesToHemisphere(1.0f);
}

Vector3 AmbientOccluder::GetDirection(ShadeRec& sr)
{
    auto sp = mpSampler->SampleHemisphere();
    return sp.x * mU + sp.y * mV + sp.z * mW;
}

Color AmbientOccluder::L(ShadeRec& sr)
{
    mW = sr.normal;
    //Up vector little jittered to prevent error when mW is vertical
    mV = glm::normalize(glm::cross(mW, Vector3(0.0072f, 1.0f, 0.0034)));
    mU = glm::cross(mV, mW);

    Ray shadowRay(sr.hitPoint, this->GetDirection(sr));

    if(this->InShadow(shadowRay, sr)) {
        return mMinAmount * mLs * mColor;
    }
    return mLs * mColor;
}

bool AmbientOccluder::InShadow(Ray ray, const ShadeRec& sr)
{
    Real t;

    for(const auto& pObj : sr.world.GetObjects()) {
        if(pObj->ShadowHit(ray, t)) {
            return true;
        }
    }
    return false;
}

} //end of namespace
