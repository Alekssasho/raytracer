//
//  EnvironmentLight.h
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/28/14.
//
//

#ifndef __RayTracer__EnvironmentLight__
#define __RayTracer__EnvironmentLight__

#include "Light.h"
#include "../Samplers/Sampler.h"
#include "../Materials/Material.h"

namespace RayTracer
{
class EnvironmentLight : public Light
{
public:
    EnvironmentLight();

    void SetSampler(Sampler* ps);
    void SetMaterial(Material* pMat);

    Vector3 GetDirection(ShadeRec& sr) override;
    Color L(ShadeRec& sr) override;
    bool InShadow(Ray ray, const ShadeRec& sr) override;
    Real Pdf(const ShadeRec& sr) const override;

private:
    SamplerPtr mpSampler;
    MaterialPtr mpMaterial;
    Vector3 mU, mV, mW;
    Vector3 mWi;
};
} //end of namespace

#endif /* defined(__RayTracer__EnvironmentLight__) */
