//
//  Ambient.cpp
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/22/14.
//
//

#include "Ambient.h"

namespace RayTracer
{
Ambient::Ambient()
    : Light()
    , mLs(1.0f)
    , mColor(gWhite)
{
}

Vector3 Ambient::GetDirection(ShadeRec& sr)
{
    return Vector3(0.0f);
}

Color Ambient::L(ShadeRec& sr)
{
    return mLs * mColor;
}
} //end of namespace