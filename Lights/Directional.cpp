//
//  Directional.cpp
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/22/14.
//
//

#include "Directional.h"
#include "../World/World.h"

namespace RayTracer
{
Directional::Directional()
    : mLs(1.0f)
    , mColor(gWhite)
    , mDirection(gUp)
{
}

Vector3 Directional::GetDirection(ShadeRec& sr)
{
    return mDirection;
}

Color Directional::L(ShadeRec& sr)
{
    return mLs * mColor;
}

bool Directional::InShadow(Ray ray, const ShadeRec& sr)
{
    Real t;

    for(const auto& pObj : sr.world.GetObjects()) {
        if(pObj->ShadowHit(ray, t))
            return true;
    }
    return false;
}
} //end of namespace