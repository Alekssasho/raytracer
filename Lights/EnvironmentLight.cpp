//
//  EnvironmentLight.cpp
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/28/14.
//
//

#include "EnvironmentLight.h"
#include "../World/World.h"

namespace RayTracer
{
EnvironmentLight::EnvironmentLight()
    : Light()
{
}

void EnvironmentLight::SetSampler(Sampler* ps)
{
    mpSampler.reset(ps);
    mpSampler->MapSamplesToHemisphere(1.0f);
}

void EnvironmentLight::SetMaterial(Material* pMat)
{
    mpMaterial.reset(pMat);
}

Vector3 EnvironmentLight::GetDirection(ShadeRec& sr)
{
    mW = sr.normal;
    //Jittered up vector to prevent zeroed mV when mW is vertical
    mV = glm::normalize(glm::cross(Vector3(0.0034f, 1.0f, 0.0071f), mW));
    mU = glm::cross(mV, mW);
    auto sp = mpSampler->SampleHemisphere();
    mWi = sp.x * mU + sp.y * mV + sp.z * mW;

    return mWi;
}

Color EnvironmentLight::L(ShadeRec& sr)
{
    return mpMaterial->GetL(sr);
}

bool EnvironmentLight::InShadow(Ray ray, const ShadeRec& sr)
{
    Real t;

    for(const auto& pObj : sr.world.GetObjects()) {
        if(pObj->ShadowHit(ray, t)) {
            return true;
        }
    }
    return false;
}

Real EnvironmentLight::Pdf(const ShadeRec& sr) const
{
    return glm::dot(sr.normal, mWi) * INV_PI;
}
} //end of namespace