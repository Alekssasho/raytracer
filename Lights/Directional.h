//
//  Directional.h
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/22/14.
//
//

#ifndef __RayTracer__Directional__
#define __RayTracer__Directional__

#include "Light.h"

namespace RayTracer
{
class Directional : public Light
{
public:
    Directional();

    Vector3 GetDirection(ShadeRec& sr) override;
    Color L(ShadeRec& sr) override;
    bool InShadow(Ray ray, const ShadeRec& sr) override;

    void ScaleRadiance(Real ls) { mLs = ls; }
    void SetColor(Color c) { mColor = c; }
    void SetDirection(Vector3 dir) { mDirection = glm::normalize(dir); }

private:
    Real mLs;
    Color mColor;
    Vector3 mDirection;
};
} //end of namespace

#endif /* defined(__RayTracer__Directional__) */
