//
//  Ambient.h
//  RayTracer
//
//  Created by Aleksandar Angelov on 7/22/14.
//
//

#ifndef __RayTracer__Ambient__
#define __RayTracer__Ambient__

#include "Light.h"

namespace RayTracer
{
class Ambient : public Light
{
public:
    Ambient();

    Vector3 GetDirection(ShadeRec& sr) override;
    Color L(ShadeRec& sr) override;

    void ScaleRadiance(Real ls) { mLs = ls; }
    void SetColor(Color c) { mColor = c; }

private:
    Real mLs;
    Color mColor;
};
} //end of namespace

#endif /* defined(__RayTracer__Ambient__) */
